(defpackage :lsp-server/test-utils
  (:use :gt/full
        :lsp-server
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server.lem-base)
  (:import-from :lsp-server :server-buffer-name :document)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :with-temporary-server
           :with-temporary-buffer))
(in-package :lsp-server/test-utils)
(in-readtable :curry-compose-reader-macros)

(defun call/temporary-server (fn &key (server-class 'lsp-server))
  (assert (subtypep server-class 'lsp-server))
  (let* ((*server* (make-instance server-class)))
    (funcall fn)))

(defmacro with-temporary-server ((&rest args &key &allow-other-keys)
                                 &body body)
  `(call/temporary-server (lambda () ,@body) ,@args))

(defun call/temporary-buffer (fn &key (server-class 'lsp-server)
                                   (text "")
                                   (uri "file://tmp/no-such-file"))
  (call/temporary-server
   (lambda ()
     (let* ((buffer (make-buffer (server-buffer-name uri)
                                 :filename (uri-to-filename uri)
                                 :enable-undo-p nil)))
       (insert-string (buffer-point buffer) text)
       (funcall fn buffer)))
   :server-class server-class))

(defmacro with-temporary-buffer ((buffer &rest args &key &allow-other-keys)
                                 &body body)
  `(call/temporary-buffer
    (lambda (,buffer)
      ,@body)
    ,@args))
