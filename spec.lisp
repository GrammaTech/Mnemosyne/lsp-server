(defpackage :lsp-server/spec
  (:use :gt/full)
  (:import-from :asdf :system-relative-pathname)
  (:export :valid-lsp-string?
           :validate-lsp-symbols
           :cap
           :cap-path
           :cap-optional?
           :cap-type
           :cap-method
           :server-cap
           :client-cap
           :lsp-spec-text
           :*client-caps*
           :*server-caps*)
  (:documentation "Static validation against the LSP spec."))
(in-package :lsp-server/spec)

(defvar-unbound *lsp-spec-text*
  "Lazily-loaded text of the LSP specification.
Lazy because it is only needed when compiling.")

(defun lsp-spec-text ()
  "Get the cached text of the LSP spec."
  (or (bound-value '*lsp-spec-text*)
      (synchronized ('*lsp-spec-text*)
        (or (bound-value '*lsp-spec-text*)
            (setf *lsp-spec-text*
                  (read-in-lsp-spec))))))

(defun read-in-lsp-spec (&aux file-strings)
  "Read in the text of the LSP spec from disk."
  (walk-directory (system-relative-pathname
                   :lsp-server
                   "etc/specification-3-17"
                   :type :directory)
                  (lambda (filename)
                    (when (equal "md" (pathname-type filename))
                      (push (read-file-into-string filename
                                                   :external-format :utf-8)
                            file-strings))))
  (apply #'string+ file-strings))

(defun valid-lsp-string? (string)
  "Does STRING occur (as a word) in the LSP spec?"
  (let ((string (string string)))
    (and (not (string$= string "?"))
         (or (member string '(|WorkspaceClientCapabilities|
                              |WindowClientCapabilities|)
                     :test #'string=)
             (scan (list :sequence :word-boundary string :word-boundary)
                   (lsp-spec-text)))
         t)))

(defun validate-lsp-symbols (symbols)
  "Check that SYMBOLS are all present in the LSP spec."
  (dolist (s symbols)
    (assert (valid-lsp-string? s) ()
            "The symbol ~a does not appear in LSP." s)))

(defclass cap ()
  ((property-path :initarg :property-path :type string :reader cap-path)
   (optional :initarg :optional :type boolean :reader cap-optional?)
   (property-type :initarg :property-type :type string :reader cap-type)
   (method :initarg :method :type string :reader cap-method)
   (result :initarg :result :type string :reader cap-result)
   (param :initarg :result :type string :reader cap-param))
  (:documentation "Representation for a parsed capabilitiy.")
  (:default-initargs
   :optional t
   :property-type t))

(defmethod print-object ((cap cap) stream)
  (print-unreadable-object (cap stream :type t)
    (with-slots (property-path property-type optional method) cap
      (format stream "~a ~@[~a ~]~a ~a"
              method optional property-path property-type)))
  cap)

(defclass server-cap (cap)
  ()
  (:documentation "A server capability."))

(defclass client-cap (cap)
  ()
  (:documentation "A client capability."))

(-> client-header? (string) boolean)
(defun client-header? (line)
  (true (search "_Client Capability_:" line)))

(-> server-header? (string) boolean)
(defun server-header? (line)
  (true (search "_Server Capability_:" line)))

(-> parse-property-path (string)
    (or null (tuple string (or null string))))
(defun parse-property-path (line)
  (match line
    ((ppcre "\\* property (?:path|name)( \\(optional\\))?: `(.*?)`"
            optional path)
     (list path optional))))

(-> parse-property-type (string) (or null string))
(defun parse-property-type (line)
  (match line
    ((ppcre "\\* property type:? `(.*?)`" path)
     path)))

(-> parse-method (string) (or null string))
(defun parse-method (line)
  (match line
    ((ppcre "method: '(.*?)'" m) m)
    ((ppcre "method: `(.*?)`" m) m)))

(defun extract-words (s)
  "Like `words', but also splits camelCase."
  (mappend (lambda (word)
             (runs word :test (lambda (x y)
                                (and (lower-case-p y)
                                     (or (upper-case-p x)
                                         (lower-case-p y))))))
           (words s)))

(-> extract-caps ((or symbol class)
                  (-> (string) boolean)
                  string)
    list)
(defun extract-caps (class keep? text)
  "Extract capabilities from TEXT, return instances of CLASS."
  (let* ((class (find-class class))
         (lines (remove-if #'emptyp (lines text)))
         (sections
          (drop-until (compose keep? #'car)
                      (runs lines :key keep?))))
    (nlet rec ((sections sections)
               (acc '()))
      (match sections
        ((list) (nreverse acc))
        ((list* _ data rest)
         (let* ((data
                 (take-until (disjoin keep? (op (string^= "####" _)))
                             data))
                (path (some #'parse-property-path data))
                (type (some #'parse-property-type data))
                (methods (filter-map #'parse-method data)))
           (cond ((or path type)
                  (rec rest
                       (append
                        (mapcar
                         (lambda (method)
                           (unless (intersection
                                    (extract-words (first path))
                                    (extract-words method)
                                    :test #'string-equal)
                             (error "Implausible path ~a for method ~a."
                                    (first path) method))
                           (make class
                                 :property-path (first path)
                                 :optional (true (second path))
                                 :property-type type
                                 :method
                                 (or method
                                     (error "No method for capability!"))))
                         methods)
                        acc)))
                 ((find-if (curry #'search "See general synchronization")
                           data)
                  (rec rest acc))
                 (t
                  (error "~
No property path/type or cross reference for methods ~a" methods)))))))))

(defun extract-client-caps (&optional (text (lsp-spec-text)))
  "Extract client capabiliities from TEXT."
  (extract-caps 'client-cap #'client-header? text))

(defun extract-server-caps (&optional (text (lsp-spec-text)))
  "Extract server capabilities from TEXT."
  (extract-caps 'server-cap #'server-header? text))

(-> caps-table (list) hash-table)
(defun caps-table (caps)
  "Build a table from a list of capabilities."
  (lret ((dict (dict)))
    (dolist (cap caps)
      (when-let (method (cap-method cap))
        (setf (gethash method dict) cap)))))

(defparameter *client-caps*
  (caps-table (extract-client-caps)))

(defparameter *server-caps*
  (caps-table (extract-server-caps)))
