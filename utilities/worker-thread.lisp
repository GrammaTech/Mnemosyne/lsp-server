;;; Worker threads with the appropriate LSP dynamic environment
(defpackage :lsp-server/utilities/worker-thread
  (:use :bordeaux-threads
        :lparallel
        :jsonrpc
        :jsonrpc/connection
        :lsp-server
        :lsp-server/utilities/lsp-utilities
        :gt/full)
  (:export :*debug-workers*
           :make-lsp-worker-thread
           :with-lsp-worker-thread))
(in-package :lsp-server/utilities/worker-thread)

(defvar *debug-workers* nil
  "Set to non-nil if you want to get backtraces from worker threads
in your REPL.")

(defun output-condition (worker-name condition &key (type :info))
  "Output the CONDITION from WORKER-NAME at the given keyword TYPE level."
  (let ((string (fmt "~a: ~a" worker-name condition)))
    (when (bound-value '*connection*)
      (show-message string :type type))
    (format *error-output* "~&~a~%" string)))

(defgeneric lsp-worker-function (worker-name worker)
  (:documentation "Wrap WORKER as a function with an appropriate
dynamic environment.")
  (:method ((worker-name string)
            (worker function))
    (dynamic-closure
     '(*connection*
       *server*
       *kernel*
       *trace-output*
       *standard-output*
       *error-output*)
     (named-lambda retry ()
       (restart-case
           (handler-bind
               ((serious-condition
                 ;; If there is an error in the worker,
                 ;; instead of crashing Lisp, send an error
                 ;; message to the user. TODO Let the user
                 ;; interatively request a retry?
                 (lambda (e)
                   (output-condition worker-name e :type :error)
                   (unless *debug-workers* (abort))))
                (warning
                 (lambda (w)
                   (output-condition worker-name w :type :warning))))
             (funcall worker))
         (abort ()
           :report (lambda (s)
                     (format s "Give up on ~a" worker-name))
           (return-from retry))
         (retry ()
           :report "Try again"
           (retry)))))))

(defgeneric make-lsp-worker-thread (fn &key name)
  (:documentation "Launch a worker thread, taking care to preserve the
relevant parts of the LSP server dynamic environment.")
  (:method ((function function) &key (name (string (gensym "THREAD"))))
    (make-thread (lsp-worker-function name function)
                 :name name)))

(defmacro with-lsp-worker-thread ((&rest args) &body body)
  "Launch a worker thread to execute BODY, taking care to preserve the
relevant parts of the LSP server dynamic environment."
  `(make-lsp-worker-thread (lambda () ,@body) ,@args))
