;;; LSP utilities
(defpackage :lsp-server/utilities/lsp-utilities
  (:use :quri
        :jsonrpc
        :jsonrpc/class
        :jsonrpc/connection
        :lsp-server/protocol
        :lsp-server/lsp-server
        :gt/full)
  (:import-from :lsp-server.lem-base
                :buffer
                :buffer-start-point
                :buffer-end-point
                :points-to-string)
  (:export :file-uri-path
           :file-uri-pathname
           :file-path-uri
           :path-from-params
           :safe-current-connection
           :current-connection
           :show-message
           :buffer-to-string
           :file-contents-from-buffer
           :make-position
           :make-empty-range))
(in-package :lsp-server/utilities/lsp-utilities)

(-> file-uri-path (string) (values string string &optional))
(defun file-uri-path (uri)
  "Extract the file path as a string from URI."
  (let ((uri (uri uri)))
    (values (url-decode (uri-path uri))
            (uri-scheme uri))))

(-> file-uri-pathname (string) (values pathname &optional))
(defun file-uri-pathname (uri)
  "Extract a pathname from URI."
  ;; Note that Windows pathnames in URIs use a Unix-like format (e.g.
  ;; `file:///c:/path/to/the%20file.txt'). On Windows it might be
  ;; necessary to do more postprocessing to make sure the drive
  ;; component in parsed properly.
  (parse-unix-namestring (file-uri-path uri)))

(-> file-path-uri ((or string pathname)) (values string &optional))
(defun file-path-uri (path)
  "Convert PATH to a file URI string."
  (nest (render-uri)
        (make-uri :scheme "file" :path)
        (substitute #\+ #\Space)
        (etypecase path
          (string path)
          (pathname (unix-namestring path)))))

(-> path-from-params (t) (values pathname &optional))
(defun path-from-params (params)
  "Return the path to the document in LSP PARAMS."
  (file-uri-pathname (slot-value (slot-value params '|textDocument|) '|uri|)))

(-> initialize-params-safe ()
    (values (or null |InitializeParams|)
            (or null error)
            &optional))
(defun initialize-params-safe ()
  "Get the current initialization parameters, or nil."
  (ignore-some-conditions (unbound-variable unbound-slot)
    (let ((*connection* (or (safe-current-connection)
                            *connection*)))
      (values (initialize-params*) nil))))

(-> safe-current-connection (&key (:server lsp-server))
                            (values (or connection null) &optional))
(defun safe-current-connection (&key (server *server*))
  "Get the current connection, return nil if it cannot be found."
  (or (bound-value '*connection*)
      (car (server-client-connections server))))

(-> current-connection (&key (:server lsp-server))
                       (values connection &optional))
(defun current-connection (&key (server *server*))
  "Get the current connection, erroring if it cannot be found."
  (or (safe-current-connection :server server)
      (error "No connections for ~a" server)))

(deftype message-type-keyword ()
  '(member :error :warning :info :log))

(-> show-message (string &key
                  (:type message-type-keyword)
                  (:server (or null lsp-server)))
    (values t &optional))
(defun show-message (message &key (type :info) (server *server*)
                     &aux (*connection* (current-connection)))
  (let ((type
         (ecase-of message-type-keyword type
           (:error |MessageType.Error|)
           (:warning |MessageType.Warning|)
           (:info |MessageType.Info|)
           (:log |MessageType.Log|))))
    (notify server
            "window/showMessage"
            (dict "type" type "message" message))))

(-> buffer-to-string (buffer) (values string &optional))
(defun buffer-to-string (buffer)
  (let ((start (buffer-start-point buffer))
        (end (buffer-end-point buffer)))
    (points-to-string start end)))

(-> file-contents-from-buffer (pathname)
                              (values (or string null) &optional))
(defun file-contents-from-buffer (path)
  "Return the contents of the file buffer at PATH."
  (when-let* ((*connection* (current-connection))
              (buffer (get-buffer-from-uri (file-path-uri path))))
    (buffer-to-string buffer)))

(-> make-position (number &optional number) |Position|)
(defun make-position (lineno &optional (column 1))
  "Create a position point at LINENO and COLUMN (1-indexed)."
  (make '|Position| :|line| lineno :|character| column))

(-> make-empty-range (|Position|) |Range|)
(defun make-empty-range (pos)
  "Create an empty range at POS."
  (make '|Range| :|start| pos :|end| pos))
