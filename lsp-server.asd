(defsystem "lsp-server"
  :depends-on ("bordeaux-threads"
               "usocket"
               "cl-ppcre"
               "optima"
               "alexandria"
               "serapeum"
               "trivial-types"
               "closer-mop"
               "quri"
               "jsonrpc"
               "yason"
               "gt/full"
               "lsp-server/lem-base")
  :in-order-to ((test-op (test-op "lsp-server/test")))
  :serial t
  :components ((:file "state")
               (:file "spec")
               (:file "logger")
               (:file "protocol")
               (:file "protocol-util")
               (:file "lsp-server")))

(defsystem "lsp-server/lem-base"
  :depends-on ("uiop"
               "iterate"
               "alexandria"
               "cl-ppcre"
               "cl-annot"
               "bordeaux-threads"
               "serapeum"
               "named-readtables"
               "lparallel"
               "infix-math")
  :pathname "lem-base/"
  :serial t
  :components ((:file "ordered-dict")
               (:file "package")
               (:file "readtable")
               (:file "documentation")
               (:file "util")
               (:file "errors")
               (:file "var")
               (:file "wide")
               (:file "macros")
               (:file "hooks")
               (:file "line")
               (:file "buffer")
               (:file "buffer-insert")
               (:file "buffers")
               (:file "point")
               (:file "basic")
               (:file "syntax")
               (:file "file")
               (:file "search")
               (:file "indent")))

(defsystem "lsp-server/utilities"
  :depends-on ("bordeaux-threads"
               "quri"
               "jsonrpc"
               "gt/full"
               "lsp-server/lem-base"
               "lsp-server")
  :pathname "utilities"
  :serial t
  :components ((:file "lsp-utilities")
               (:file "worker-thread")))

(defsystem "lsp-server/test-utils"
  :description "Auxiliary system for other systems to depend on
  without changing the Stefil root."
  :depends-on ("lsp-server")
  :serial t
  :components ((:file "test-utils")))

(defsystem "lsp-server/test"
  :depends-on ("lsp-server"
               "lsp-server/utilities"
               "lsp-server/test-utils"
               "stefil+"
               "software-evolution-library/utility/git")
  :serial t
  :perform (test-op (o c) (symbol-call :lsp-server/test '#:run-batch))
  :components ((:file "test")))
