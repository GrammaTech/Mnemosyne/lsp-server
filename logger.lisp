(defpackage :lsp-server/logger
  (:use :cl :lsp-server/state)
  (:import-from :serapeum
                :output-stream
                :defplace
                :synchronized
                :defvar-unbound
                :etypecase-of
                :string-case)
  (:export :logger-stream
           :logger-stream*
           :*logger-stream*
           :log-format
           :with-log-file
           :*server*
           :log-file-designator
           :parse-log-file-options))
(in-package :lsp-server/logger)

(deftype log-file-designator ()
  '(or output-stream
    null
    (eql t)                             ;stdio
    (eql 1)                             ;stdio
    (eql 2)                             ;stderr
    string
    pathname))

(defgeneric logger-stream (server))

(defvar *logger-stream* nil
  "The default stream to log to.")

(defplace logger-stream* ()
  (logger-stream *server*))

(defun log-format (string &rest args &aux (stream (logger-stream*)))
  (when stream
    (synchronized (stream)
      (apply #'format stream string args)
      (force-output stream))))

(defun call-with-log-file (file fn)
  (etypecase-of log-file-designator file
    (output-stream
     (let ((*logger-stream* file))
       (funcall fn)))
    (null
     (let ((*logger-stream* nil))
       (funcall fn)))
    ((member t 1)
     (let ((*logger-stream* (make-synonym-stream '*standard-output*)))
       (funcall fn)))
    ((eql 2)
     (let ((*logger-stream* (make-synonym-stream 'uiop:*stderr*)))
       (funcall fn)))
    (string
     (call-with-log-file (merge-pathnames file) fn))
    (pathname
     (with-open-file (*logger-stream*
                      file
                      #+ccl :sharing #+ccl :lock
                      :direction :output
                      :if-does-not-exist :create
                      :if-exists :append)
       (funcall fn)))))

(defmacro with-log-file ((file &rest kwargs &key &allow-other-keys)
                         &body body)
  `(call-with-log-file ,file (lambda () ,@body) ,@kwargs))

(defun parse-log-file-options (log-file no-log)
  "Parse log file options given at the command line for the LSP server."
  (cond (no-log nil)
        ((not log-file) 1)
        ((stringp log-file)
         (string-case log-file
           (("STDOUT" "-" "t") t)
           (("2") 2)))
        (t log-file)))
