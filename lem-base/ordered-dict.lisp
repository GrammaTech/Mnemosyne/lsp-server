(defpackage :lsp-server/ordered-dict
  (:use :gt/full :infix-math)
  (:export :ordered-dict)
  (:shadowing-import-from :infix-math :$)
  (:shadowing-import-from :iterate :with)
  (:export :ordered-dict
           :synchronized-ordered-dict
           :make-ordered-dict
           :clear :getitem :delitem :popitem))
(in-package :lsp-server/ordered-dict)

;;; This is a very literal translation of the ordered dictionary
;;; implementation at <https://code.activestate.com/recipes/578375/>
;;; (MIT license), which is the basis for the ordered dictionary
;;; implementation currently used in Python.

(defconst +free+ -1)
(defconst +dummy+ -2)

(defconst +perturb-shift+ 5)

(defun gen-probes (hashvalue mask)
  "Return an interator that implements a probe sequence."
  (declare (fixnum hashvalue) (array-index mask))
  (let* ((first t) (i 0) (perturb 0)
         (y (named-lambda yield ()
              (when (minusp hashvalue)
                (callf #'- hashvalue))
              (setf i (logand hashvalue mask))
              (when first
                (setf first nil)
                (return-from yield
                  (prog1 i
                    (setf perturb hashvalue))))
              (setf i ($ (5 * i + perturb + 1) logand #xFFFFFFFFFFFFFFFF))
              (return-from yield
                (prog1 (logand i mask)
                  (callf #'ash perturb (- +perturb-shift+)))))))
    (declare ((unsigned-byte 64) i perturb))
    (lambda (arg)
      (ecase arg
        (:done? nil)
        (:more? t)
        (:get (values (funcall y) t))))))

(defun _make-index (n)
  "New sequence of indices using the smallest possible datatype."
  (make-array n
              :initial-element +free+
              :element-type `(integer ,+dummy+ ,n)))

;;; TODO This could be made functional by substituting FSet seqs for
;;; vectors.

(defclass ordered-dict ()
  ((hash-fn :initarg :hash-fn :type function :reader hash-fn)
   (test-fn :initarg :test-fn :type function :reader test-fn)
   (filled :initform 0 :type (integer 0 *))
   (used :initform 0 :type (integer 0 *))
   (indices :initform (_make-index 8) :type vector)
   (keylist :initform (vect) :type vector)
   (hashlist :initform (vect) :type vector)
   (valuelist :initform (vect) :type vector))
  (:default-initargs
   :hash-fn #'sxhash
   :test-fn #'eql))

(defmethods ordered-dict (self filled used
                               indices
                               keylist hashlist valuelist
                               hash-fn test-fn)
  (:method _lookup (self key hashvalue)
    (assert (< filled (length indices)))
    (fbind ((test test-fn))
      (iter (with freeslot = nil)
            (for i in-iterator (gen-probes hashvalue (1- (length indices))))
            (for index = (aref indices i))
            (cond ((eq index +free+)
                   (return
                     (if freeslot
                         (values +dummy+ freeslot)
                         (values +free+ i))))
                  ((eq index +dummy+)
                   (when (null freeslot)
                     (setf freeslot i)))
                  ((or (eq (aref keylist index) key)
                       (and (eql (aref hashlist index) hashvalue)
                            (test (aref keylist index) key)))
                   (return (values index i)))))))
  (:method resize (self n)
    "Reindex the existing hash/key/value entries.
Entries do not get moved, they only get new indices.
No hashing and no equality testing is done."
    (setf n (expt 2 (integer-length n))
          indices (_make-index n))
    (iter (for hashvalue in-vector hashlist)
          (for index from 0)
          (iter (for i in-iterator (gen-probes hashvalue (1- n)))
                ;; NB The Python is confusing here.
                (when (eq +free+ (aref indices i))
                  (setf (aref indices i) index)
                  (return))
                (finally (setf (aref indices i) index))))
    (setf filled used))
  (:method clear (self)
    (setf indices (_make-index 8)
          hashlist (vect)
          keylist (vect)
          valuelist (vect)
          used 0
          filled 0))
  (:method getitem (self key)
    (let* ((hashvalue (funcall hash-fn key))
           (index (_lookup self key hashvalue)))
      (if (< index 0)
          (values nil nil)
          (values (aref valuelist index) t))))
  (:method (setf getitem) (value self key)
    (setitem self key value))
  (:method setitem (self key value)
    (mvlet* ((hashvalue (funcall hash-fn key))
             (index i (_lookup self key hashvalue)))
      (if (< index 0)
          (progn
            (setf (aref indices i) used)
            (vector-push-extend hashvalue hashlist)
            (vector-push-extend key keylist)
            (vector-push-extend value valuelist)
            (incf used)
            (when (eq index +free+)
              (incf filled)
              (when (> (* filled 3) (* (length indices) 2))
                (resize self (* 4 (size self))))))
          (setf (aref valuelist index) value))))
  (:method delitem (self key)
    (mvlet* ((hashvalue (funcall hash-fn key))
             (index i (_lookup self key hashvalue)))
      (unless (< index 0)
        (setf (aref indices i) +dummy+)
        (decf used)
        (when (/= index used)
          ;; Swap with the last entry to avoid leaving a hole.
          (mvlet* ((lasthash (last-elt hashlist))
                   (lastkey (last-elt keylist))
                   (lastvalue (last-elt valuelist))
                   (lastindex j (_lookup self lastkey lasthash)))
            (assert (and (>= lastindex 0) (/= i j)))
            (setf (aref indices j) index
                  (aref hashlist index) lasthash
                  (aref keylist index) lastkey
                  (aref valuelist index) lastvalue)))
        (vector-pop hashlist)
        (vector-pop keylist)
        (vector-pop valuelist)
        t)))
  (:method popitem (self)
    (when (emptyp keylist)
      (error "Dictionary is empty"))
    (let ((key (last-elt keylist))
          (value (last-elt keylist)))
      (delitem self key)
      (values key value)))
  (:method size (self)
    used)
  (:method iterator (self &key)
    (iterator keylist))
  (:method contains? (self key &optional v)
    (declare (ignore v))
    (>= (nth-value 1 (_lookup self key (funcall hash-fn key)))
        0))
  (:method lookup (self key)
    (let ((index (_lookup self key (funcall hash-fn key))))
      (if (>= index 0)
          (values (aref valuelist index) t)
          (values nil nil))))
  (:method convert ((to-type (eql 'alist)) self &key)
    (map 'list #'cons keylist valuelist))
  (:method convert ((to-type (eql 'fset:alist)) self &key)
    (map 'list #'cons keylist valuelist))
  (:method domain (self)
    (convert 'set keylist))
  (:method range (self)
    (convert 'set valuelist))
  (:method print-object (self stream)
    (print-unreadable-object (self stream :type t)
      (format stream "~s" (convert 'alist self)))))

(defclass synchronized-ordered-dict (ordered-dict)
  ((lock :initform (bt:make-recursive-lock "Ordered dict lock") :reader monitor)))

(defmethods synchronized-ordered-dict (self keylist)
  (:method _lookup :around (self key hashvalue)
    (synchronized (self)
      (call-next-method)))
  (:method resize :around (self n)
    (synchronized (self)
      (call-next-method)))
  (:method clear :around (self)
    (synchronized (self)
      (call-next-method)))
  (:method getitem :around (self key)
    (synchronized (self)
      (call-next-method)))
  (:method setitem :around (self key value)
    (synchronized (self)
      (call-next-method)))
  (:method delitem :around (self key)
    (synchronized (self)
      (call-next-method)))
  (:method popitem :around (self)
    (synchronized (self)
      (call-next-method)))
  (:method iterator (self &key)
    (iterator (copy-seq keylist)))
  (:method convert :around (to-type self &key)
    (synchronized (self)
      (call-next-method))))

(defun ordered-dict (&rest args)
  (lret ((dict (make 'ordered-dict :test-fn #'equal)))
    (iter (for (key value) on args by #'cddr)
          (setf (getitem dict key) value))))

(defun make-ordered-dict (&key (test #'eql)
                            (hash-function #'sxhash)
                            synchronized
                            initial-contents)
  (lret ((dict
          (make (if synchronized 'synchronized-ordered-dict 'ordered-dict)
                :test-fn (ensure-function test)
                :hash-fn (ensure-function hash-function))))
    (iter (for (key . value) in initial-contents)
          (setf (getitem dict key) value))))
