(in-package :lsp-server.lem-base)

(defreadtable lem
  (:merge :standard)
  (:macro-char #\@ #'cl-annot.syntax:annotation-syntax-reader))
