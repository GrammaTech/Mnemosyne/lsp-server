(uiop:define-package :lsp-server.lem-base
    (:use :cl :lsp-server/ordered-dict)
  (:local-nicknames (:lp :lparallel))
  (:import-from :alexandria :when-let :if-let :once-only)
  (:import-from :serapeum :synchronized :-> :assure :lret)
  (:import-from :named-readtables :in-readtable :defreadtable))
