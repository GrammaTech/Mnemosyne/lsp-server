(in-package :lsp-server.lem-base)

(in-readtable lem)

(export '(kill-buffer-hook
          buffer-list
          ghost-buffer-p
          special-buffer-p
          filter-special-buffers
          any-modified-buffer-p
          get-buffer
          get-buffer-create
          uniq-buffer-name
          update-prev-buffer
          bury-buffer
          get-next-buffer
          delete-buffer
          get-file-buffer
          get-buffer-promise))

(define-editor-variable kill-buffer-hook '())

(defvar *buffer-list*
  (make-ordered-dict :test 'equal :synchronized t))

(deftype promise ()
  '#.(type-of (lp:promise)))

(defmacro with-buffer-list-locked ((b &key) &body body)
  `(let ((,b *buffer-list*))
     (synchronized (,b)
       ,@body)))

(defstruct wrapper
  (name (alexandria:required-argument :name)
   :type string
   :read-only t)
  (promise
   (lp:promise)
   :type promise))

(defun wrap-buffer (buffer)
  (make-wrapper
   :name (buffer-name buffer)
   :promise (lret ((p (lp:promise)))
              (lp:fulfill p buffer))))

(defun unwrap-buffer (wrapper)
  (let ((p (wrapper-promise wrapper)))
    (and (lp:fulfilledp p)
         (lp:force p))))

(defun add-buffer (buffer)
  (check-type buffer buffer)
  (unless (ghost-buffer-p buffer)
    (with-buffer-list-locked (_)
      (assert (not (get-buffer (buffer-name buffer))))
      (let ((promise (get-buffer-promise (buffer-name buffer))))
        (lp:fulfill promise buffer))
      (buffer-list))))

(defun buffer-list ()
  @lang(:jp "`buffer`のリストを返します。")
  (loop for (k . w) in (fset:convert 'fset:alist *buffer-list*)
        for b = (unwrap-buffer w)
        if b collect it))

(defun ghost-buffer-p (buffer)
  (let ((name (buffer-name buffer)))
    (and (<= 3 (length name))
         (char= #\space (aref name 0))
         (char= #\* (aref name 1))
         (char= #\* (aref name (1- (length name)))))))

(defun special-buffer-p (buffer)
  (or (ghost-buffer-p buffer)
      (let ((name (buffer-name buffer)))
        (and (char= #\* (aref name 0))
             (char= #\* (aref name (1- (length name))))))))

(defun filter-special-buffers ()
  (remove-if #'special-buffer-p (buffer-list)))

(defun any-modified-buffer-p ()
  (find-if #'(lambda (buffer)
               (and (buffer-filename buffer)
                    (buffer-modified-p buffer)))
           (filter-special-buffers)))

(-> get-buffer-promise (string) promise)
(defun get-buffer-promise (name)
  "Get a promise for NAME, a buffer name.
The promise will be fulfilled if and when the buffer is actually
created."
  (wrapper-promise
   (with-buffer-list-locked (bl)
     (or (getitem bl name)
         (lret ((w (make-wrapper :name name)))
           (setf (getitem bl name) w))))))

(defun get-buffer (buffer-or-name)
  @lang(:jp "`buffer-or-name`がバッファならそのまま返し、
文字列ならその名前のバッファを返します。")
  (if (bufferp buffer-or-name)
      buffer-or-name
      (find-if #'(lambda (buffer)
                   (string= buffer-or-name
                            (buffer-name buffer)))
               (buffer-list))))

(defun get-buffer-create (name)
  @lang(:jp "バッファ名`name`のバッファがあればそれを返し、
無ければ作って返します。")
  (handler-bind ((buffer-already-exists #'continue))
    (make-buffer name)))

(defun uniq-buffer-name (name)
  (if (null (get-buffer name))
      name
      (do ((n 1 (1+ n))) (nil)
        (let ((name (format nil "~a<~d>" name n)))
          (unless (get-buffer name)
            (return name))))))

(defun delete-buffer (buffer)
  @lang(:jp "`buffer`をバッファのリストから消します。
エディタ変数`kill-buffer-hook`がバッファが消される前に実行されます。")
  (check-type buffer buffer)
  (when-let ((hooks (variable-value 'kill-buffer-hook :buffer buffer)))
    (run-hooks hooks buffer))
  (when-let ((hooks (variable-value 'kill-buffer-hook :global)))
    (run-hooks hooks buffer))
  (buffer-free buffer)
  (with-buffer-list-locked (b)
    (delitem b (buffer-name buffer))))

(defun get-next-buffer (buffer)
  @lang(:jp "バッファリスト内にある`buffer`の次のバッファを返します。")
  (check-type buffer buffer)
  (let* ((buffer-list (reverse (buffer-list)))
         (res (member buffer buffer-list)))
    (if (cdr res)
        (cadr res)
        (car buffer-list))))

(defun bury-buffer (buffer)
  @lang(:jp "`buffer`をバッファリストの一番最後に移動させ、バッファリストの先頭を返します。")
  (check-type buffer buffer)
  (let ((name (buffer-name buffer)))
    (with-buffer-list-locked (bl)
      (when-let (wrapper (getitem bl name))
        (delitem bl name)
        (setf (getitem bl name) wrapper))))
  (car (buffer-list)))

(defun get-file-buffer (filename)
  @lang(:jp "`filename`に対応するバッファを返します。
見つからなければNILを返します。")
  (dolist (buffer (buffer-list))
    (when (uiop:pathname-equal filename (buffer-filename buffer))
      (return buffer))))
