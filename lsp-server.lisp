(defpackage :lsp-server/lsp-server
  (:use :cl
        :usocket
        :lsp-server/logger
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/logger
        :lsp-server.lem-base)
  (:local-nicknames (:lp :lparallel))
  (:import-from :lsp-server/spec :valid-lsp-string?)
  (:import-from :uiop :nest :quit)
  (:import-from :serapeum
                :->
                :string^=
                :dynamic-closure
                :fmt
                :synchronized
                :monitor
                :defplace
                :defvar-unbound
                :drop
                :standard/context
                :and-let*
                :slot-value-safe
                :dict)
  (:import-from :alexandria
                :ignore-some-conditions
                :parse-body
                :simple-style-warning
                :ensure-gethash
                :make-keyword
                :with-unique-names
                :when-let
                :when-let*
                :if-let)
  (:import-from :fset
                :convert)
  (:import-from :jsonrpc/connection
                :*connection*
                :connection)
  (:export :lsp-server
           :*server*
           :with-error-handle
           :with-address-in-use-handling
           :define-method
           :define-class-method
           :get-buffer-from-uri
           :uri-buffer
           :buffer-uri
           :buffer-version
           :buffer-language-id
           :with-document-position
           :with-text-document-position
           :notify-show-message
           :notify-log-message
           :*response-log*
           :response-log?
           :response-log
           :*request-log*
           :request-log?
           :request-log
           :initialize-params*          ;Defined in lsp-utilities
           :initialize-result*
           :handle
           :text-document-version
           :server-id
           :server-buffers
           :apply-content-changes-to-buffer
           :connection-prop
           :uri-buffer-promise
           :server-caps-support
           :server-support
           :server-support*
           :client-support
           :client-caps-support
           :client-support*
           :connection-meta
           :initialize-params-safe
           :client-info-name
           :log-backtraces?
           :log-errors?)
  (:nicknames :lsp-server)
  (:documentation "Implements a class for LSP servers and exports a default server instance."))
(in-package :lsp-server/lsp-server)

(defvar *server-id* 0
  "Counter of unique IDs for server instances.")

(defun next-server-id ()
  "Get a unique ID for a server."
  (synchronized ('*server-id*)
    (incf *server-id*)))

(defgeneric server-id (server)
  (:documentation "Get the globally unique ID for a server."))

(defclass lsp-server (jsonrpc:server)
  ((id :initform (next-server-id)
       :reader server-id
       :documentation "Prefix to distinguish buffers for this server.")
   (lock :initform (bt:make-recursive-lock "LSP server lock")
         :reader monitor)
   (request-log :initarg :request-log
                :accessor request-log?)
   (response-log :initarg :response-log
                 :accessor response-log?)
   (logger-stream :initarg :logger-stream
                  :accessor logger-stream)
   (log-errors :initarg :log-errors :reader log-errors?)
   (log-backtraces :initarg :log-backtraces :reader log-backtraces?))
  (:documentation "A JSON-RPC server that speaks the Language Server Protocol.")
  (:default-initargs
   :log-errors t
   :log-backtraces nil
   :request-log t
   :response-log t
   :logger-stream *logger-stream*))

(defmethod print-object ((self lsp-server) stream)
  (print-unreadable-object (self stream :type t :identity t)
    (format stream "#~a" (server-id self))))

(defclass lsp-connection (connection)
  ((table
    :type hash-table
    ;; Ask for the minimum hash table size (7 for SBCL, 31 on CCL).
    :initform (make-hash-table :size 0)
    :reader connection-props
    :documentation "State of an LSP connection.")))

(defclass server-info ()
  ((initialize-params)
   (initialize-result)
   (buffer-list :initform nil)
   (meta :initform (make-hash-table :test 'equal)))
  (:documentation "Per-connection server metadata."))

(-> ensure-lsp-connection (connection)
    (values lsp-connection &optional))
(defun ensure-lsp-connection (x)
  (etypecase x
    (lsp-connection x)
    (connection (change-class x 'lsp-connection))))

(defvar *server* (make-instance 'lsp-server)
  "The default LSP server.")

(-> connection-server-table ()
    (values server-info &optional))
(defun connection-server-table ()
  "Get a `server-info' instance for the current connection and server."
  (let* ((connection (ensure-lsp-connection *connection*))
         (table (connection-props connection)))
    (synchronized (connection)
      (values
       (ensure-gethash *server* table (make-instance 'server-info))))))

(defplace connection-prop (prop)
  "Look up a property by the current connection and server."
  (slot-value (connection-server-table) prop))

(defplace connection-meta (key)
  "Access arbitrary metadata for a connection."
  (gethash key (connection-prop 'meta)))

(-> initialize-params* ()
    (values (or null |InitializeParams|) &optional))
(defplace initialize-params* ()
  (connection-prop 'initialize-params))

(-> client-info-name (&key (:params (or null |InitializeParams|)))
    (values (or null string) &optional))
(defun client-info-name (&key (params (initialize-params-safe)))
  "Get clientInfo.name from the current initialization parameters."
  (and-let* (params
             (client-info (slot-value-safe params '|clientInfo|))
             (name (slot-value client-info '|name|)))
    name))

(-> initialize-result* ()
    (values (or null |InitializeResult| hash-table) &optional))
(defun initialize-result* ()
  (when-let (result (connection-prop 'initialize-result))
    (convert '|InitializeResult| result)))

(defun (setf initialize-result*) (value)
  (setf (connection-prop 'initialize-result)
        (if (null value) value
            (convert '|InitializeResult| value))))

(defun server-caps* ()
  "Get ServerCapabilities from the current server (and connection)."
  (when-let (result (initialize-result*))
    (and (slot-boundp result '|capabilities|)
         (slot-value result '|capabilities|))))

(defun server-support* (method)
  "Like `server-support' using the initialization result of the
current server and connection."
  (when-let (caps (server-caps*))
    (server-caps-support caps method)))

(defun server-support (*server* method)
  "Does SERVER support METHOD?"
  (server-support* method))

(defun client-caps* ()
  "Get ClientCapabilities for the current client."
  (when-let (result (initialize-params*))
    (and (slot-boundp result '|capabilities|)
         (slot-value result '|capabilities|))))

(defun client-support* (method)
  "Like `client-support' using the initialization parameters of the
current server and connection."
  (when-let (caps (client-caps*))
    (client-caps-support caps method)))

(defun connection-buffers ()
  (connection-prop 'buffer-list))

(defun server-buffers (server)
  "Get the buffers for SERVER (for all connections)."
  (let ((prefix (server-prefix server)))
    (remove-if-not (lambda (name)
                     (string^= prefix name))
                   (buffer-list)
                   :key #'buffer-name)))

(defun record-connection-buffer (buffer)
  (when (boundp '*connection*)
    (pushnew buffer (connection-prop 'buffer-list))))

(defvar-unbound *request-log*
  "Override whether to log requests.")

(defvar-unbound *response-log*
    "Override whether to log responses.")

(defun request-log (name params)
  "Log a request."
  (when (if (boundp '*request-log*)
            *request-log*
            (request-log? *server*))
    ;; Note that params can be "void" (shutdown, exit, etc.).
    (log-format "~%* from client~%")
    (log-format "name: ~A~%" name)
    (log-format "params: ~A~%"
                (nest
                 (with-output-to-string (stream))
                 (yason:encode params)
                 (yason:make-json-output-stream stream)))))

(defun response-log (val)
  "Log a response."
  (when (if (boundp '*response-log*)
            *response-log*
            (response-log? *server*))
    (log-format "~%* to server~%~A~%"
                (nest
                 (with-output-to-string (out))
                 (yason:encode val)
                 (yason:make-json-output-stream out)))))

(defun call-with-error-handle (server function)
  "Helper for `with-error-handle'."
  (handler-bind ((error (lambda (c)
                          (with-slots (catch-errors log-errors log-backtraces)
                              server
                            (unless (typep c 'jsonrpc:jsonrpc-error)
                              (when log-errors
                                (log-format "~A~%~@[~%~A~%~]"
                                            c
                                            (when log-backtraces
                                              (with-output-to-string (stream)
                                                (uiop:print-backtrace :stream stream
                                                                      :condition c))))))))))
    (funcall function)))

(defmacro with-error-handle (&body body)
  "Log any errors that are signaled by BODY."
  `(call-with-error-handle *server* (lambda () ,@body)))

(defmacro with-address-in-use-handling ((&key interactive-p) &body body)
  "Macro to handle port address in use errors in BODY, quitting on error unless
INTERACTIVE-P is non-NIL."
  `(handler-bind
       ((address-in-use-error
         (lambda (e) (declare (ignore e))
           (unless ,interactive-p
             (format *logger-stream*
                     "Not starting. Port is already in use.")
             (quit 1)))))
     (progn ,@body)))

(defvar *recursive* nil
  "Flag to recognize recursive calls to `handle'.")

(defgeneric handle (server method-name params)
  (:documentation "Handle a JSON-RPC request.
METHOD-NAME is a keyword.")
  (:method (server method-name params)
    (declare (ignore server method-name params))
    (error 'jsonrpc:jsonrpc-method-not-found))
  (:method :around (server method-name params)
           (if *recursive*
               (call-next-method)
               (let ((*recursive* t)
                     (*server* server)
                     (name (string method-name)))
                 (with-error-handle
                     (request-log name params)
                   (check-initialized name)
                   (let ((val (call-next-method)))
                     (response-log val)
                     val))))))

(defmethod jsonrpc:dispatch ((server lsp-server) request)
  (let* ((id (jsonrpc:request-id request))
         (method (jsonrpc:request-method request))
         (method-keyword (alexandria:make-keyword method))
         (params (jsonrpc:request-params request))
         (result
          (handler-case
              (let ((*server* server))
                (handle server method-keyword params))
            ;; TODO Get rid of this later.
            (jsonrpc:jsonrpc-method-not-found ()
              ;; "If a server or client receives notifications
              ;; starting with ‘$/’ it is free to ignore the
              ;; notification."
              (if (and (string^= "$/" method)
                       (null id))
                  nil
                  (call-next-method))))))
    (when id
      (jsonrpc:make-response :id id :result result))))

(defmacro define-method (name (params &optional params-type without-lock)
                         &body body)
  "Define a default LSP method."
  `(define-class-method ,name lsp-server (,params ,params-type ,without-lock)
     ,@body))

(defmacro define-class-method (name class
                               (params &optional params-type without-lock)
                               &body body)
  "Define an LSP method for a certain class of server."
  (when params-type
    (assert (protocol-symbol-p params-type)))
  (unless (or (valid-lsp-string? name)
              (string^= "argot/" name))
    (simple-style-warning "~a is not a known LSP method" name))
  (with-unique-names (_server _method)
    (multiple-value-bind (body decls docstring)
        (parse-body body :documentation t)
      `(progn
         ,@(when params-type
             `((defmethod handle ((s ,class)
                                  (m (eql ,(make-keyword name)))
                                  (params hash-table))
                 (handle s m (convert-from-hash-table ',params-type params)))))
         (defmethod handle ((,_server ,class)
                            (,_method (eql ,(make-keyword name)))
                            (,params ,(or params-type t)))
           ,@(and docstring (list docstring))
           ,@decls
           ,(if without-lock
                `(progn ,@body)
                `(synchronized (*server*) ,@body)))))))

(defun server-buffer-name (uri)
  "Generate a unique per-server buffer name for URI."
  (check-type uri string)
  (fmt "~a~a" (server-prefix) uri))

(defun server-prefix (&optional (server *server*))
  (fmt "<~a>" (server-id server)))

(defun get-buffer-from-uri (uri &key ((:error errorp) nil))
  "Get the buffer for a URI."
  (or (get-buffer (server-buffer-name uri))
      (and errorp
           (error "No buffer for ~a" uri))))

(defun uri-buffer (uri &key ((:error errorp) nil))
  (get-buffer-from-uri uri :error errorp))

(defun uri-buffer-promise (uri)
  (get-buffer-promise (server-buffer-name uri)))

(defun buffer-uri (buffer)
  "Extract the URI from BUFFER's name."
  (let ((name (buffer-name buffer)))
    (if-let (pos (position #\> (buffer-name buffer)))
      (drop (1+ pos) name)
      name)))

(defun buffer-document-property (buffer key)
  (let ((document (buffer-value buffer 'document)))
    (getf document key)))

(defun uri-document-property (uri key)
  (buffer-document-property (get-buffer-from-uri uri) key))

(defun uri-version (uri)
  "Get the LSP document version for URI."
  (uri-document-property uri :version))

(defun buffer-version (buffer)
  "Get the LSP document version for BUFFER."
  (buffer-document-property buffer :version))

(defun uri-language-id (uri)
  "Get the LSP language ID for URI."
  (uri-document-property uri :language-id))

(defun buffer-language-id (buffer)
  "Get the LSP language ID for BUFFER."
  (buffer-document-property buffer :language-id))

(defgeneric text-document-version (doc)
  (:method ((doc |VersionedTextDocumentIdentifier|))
    (slot-value doc '|version|))
  (:method ((doc |TextDocumentIdentifier|))
    (uri-version (slot-value doc '|uri|)))
  (:method ((uri string))
    (uri-version uri)))

(defun call-with-document-position (uri position function)
  "Helper for `with-document-position'."
  (let ((buffer (get-buffer-from-uri uri)))
    (assert (bufferp buffer))
    (let ((point (buffer-point buffer)))
      (move-to-lsp-position point position)
      (funcall function point))))

(defmacro with-document-position ((point uri position) &body body)
  "Run BODY with point at POSITION."
  `(call-with-document-position ,uri ,position (lambda (,point) ,@body)))

(defun call-with-text-document-position (text-document-position-params function)
  "Helper for with-text-document-postion."
  (let ((position (slot-value text-document-position-params '|position|))
        (uri (slot-value (slot-value text-document-position-params '|textDocument|) '|uri|)))
    (call-with-document-position uri position function)))

(defmacro with-text-document-position ((point) params &body body)
  "Like `with-document-position' for the text document in PARAMS."
  `(call-with-text-document-position ,params (lambda (,point) ,@body)))

(defun notify-show-message (type message)
  "Ask the client to display MESSAGE."
  (log-format "window/showMessage: ~A ~A~%" type message)
  (jsonrpc:notify-async *server*
                        "window/showMessage"
                        (make-instance '|ShowMessageParams|
                                        :|type| type
                                        :|message| message)))

(defun notify-log-message (type message)
  "Ask the client to log MESSAGE."
  (log-format "window/logMessage: ~A ~A~%" type message)
  (jsonrpc:notify-async *server*
                        "window/logMessage"
                        (make-instance '|LogMessageParams|
                                        :|type| type
                                        :|message| message)))

(defun check-initialized (method-name)
  "Check that the current connection is initialized."
  (when (and (string/= method-name "initialize")
             (boundp '*connection*)
             (null (initialize-params*)))
    (alexandria:plist-hash-table
     (list "code" -32002
           "message" "did not initialize")
     :test 'equal)))

(defmethod handle :before
    ((server lsp-server)
     (method (eql :|initialize|))
     (params t))
  "Store the InitializeParams from the client."
  (setf (initialize-params*)
        (convert '|InitializeParams| params)))

(defmethod jsonrpc:dispatch :context
    ((server lsp-server)
     (req jsonrpc:request))
  (let ((response (call-next-method)))
    (when (and response
               (equal (jsonrpc:request-method req)
                      "initialize"))
      (setf (initialize-result*)
            (jsonrpc:response-result response)))
    response))

(define-method "initialize" (params)
  nil)

(define-method "initialized" (params)
  nil)

(define-method "shutdown" (params)
  (dolist (b (connection-buffers))
    (delete-buffer b))
  nil)

(define-method "exit" (params)
  (values))

(define-method "workspace/didChangeConfiguration" (params)
  nil)

(define-method "workspace/didDeleteFiles" (params)
  nil)

(define-method "textDocument/didOpen" (params |DidOpenTextDocumentParams|)
  (let ((text-document
         (slot-value params
                     '|textDocument|)))
    (with-slots (|uri| |languageId| |version| |text|)
        text-document
      ;; We may wish to persistent buffer-local state (e.g. Argot
      ;; annotations) simply by not closing the buffers, which is fine
      ;; as long as the text is the same.
      (let* ((already-exists? nil)
             (buffer
              (handler-bind ((buffer-already-exists
                              (lambda (e)
                                (setq already-exists? t)
                                (continue e))))
                (make-buffer (server-buffer-name |uri|)
                             :filename (uri-to-filename |uri|)
                             :enable-undo-p nil))))
        (record-connection-buffer buffer)
        (when already-exists?
          (unless (string= |text|
                           (points-to-string (buffer-start-point buffer)
                                             (buffer-end-point buffer)))
            (erase-buffer buffer)))
        (insert-string (buffer-point buffer) |text|)
        (setf (buffer-value buffer 'document)
              (list :language-id |languageId|
                    :version |version|)))))
  (values))

(defun apply-content-changes-to-buffer (buffer content-changes)
  "Apply CONTENT-CHANGES, a list of TextDocumentContentChangeEvent
instances, to BUFFER in the order given."
  (when-let ((point (buffer-point buffer)))
    (dolist (content-change content-changes)
      (with-slots (|range| |rangeLength| |text|)
          content-change
        ;; See microsoft/language-server-protocol#9 for a discussion
        ;; of range vs. rangeLength. Note that rangeLength is now
        ;; deprecated.
        (cond ((not (slot-boundp content-change '|range|))
               (erase-buffer buffer)
               (insert-string point |text|))
              (t
               (with-slots (|start| |end|) |range|
                 (move-to-lsp-position point |start|)
                 (if (slot-boundp content-change '|rangeLength|)
                     (delete-character point |rangeLength|)
                     (let ((end (copy-point point)))
                       (move-to-lsp-position end |end|)
                       (delete-between-points point end)))
                 (insert-string point |text|))))))))

(define-method "textDocument/didChange" (params |DidChangeTextDocumentParams|)
  (let* ((text-document (slot-value params '|textDocument|))
         (version (slot-value text-document '|version|))
         (content-changes (slot-value params '|contentChanges|)))
    (when-let* ((buffer (get-buffer-from-uri (slot-value text-document '|uri|))))
      (setf (getf (buffer-value buffer 'document) :version)
            version)
      (apply-content-changes-to-buffer buffer content-changes))))

(define-method "textDocument/willSave" (params)
  )

(define-method "textDocument/willSaveWaitUntil" (params)
  )

(define-method "textDocument/didSave" (params |DidSaveTextDocumentParams|)
  (let* ((text
          (and (slot-boundp params '|text|)
               (slot-value params '|text|)))
         (text-document
          (slot-value params '|textDocument|))
         (uri
          (slot-value text-document '|uri|))
         (buffer
          (get-buffer-from-uri uri)))
    (when (and buffer text)
      (erase-buffer buffer)
      (insert-string (buffer-point buffer) text)))
  (values))

(define-method "textDocument/didClose" (params |DidCloseTextDocumentParams|)
  (let* ((text-document
          (slot-value params '|textDocument|))
         (uri
          (slot-value text-document '|uri|))
         (buffer
          (get-buffer-from-uri uri)))
    (when buffer
      (delete-buffer buffer)))
  (values))

(define-method "textDocument/codeLens" (params |CodeLensParams|)
  (vector))

(define-method "textDocument/codeAction" (params |CodeActionParams|)
  nil)

(define-method "textDocument/documentLink" (params)
  (vector))

(define-method "textDocument/hover" (params)
  nil)

(define-method "textDocument/selectionRange" (params)
  nil)

(define-method "textDocument/documentHighlight" (params)
  (vector))

(define-method "textDocument/references" (params)
  (vector))

(define-method "textDocument/signatureHelp" (params)
  nil)

(define-method "textDocument/foldingRange" (params)
  nil)

(define-method "textDocument/documentSymbol" (params)
  nil)

;;; Force SignatureHelp to be parsed.
(define-method "textDocument/signatureHelp" (params |SignatureHelpParams|)
  (call-next-method params))

(define-method "textDocument/definition" (params)
  nil)
