# lsp-server

A language-agnostic implementation of the Language Server Protocol for Common Lisp.

## Adding LSP server capabilities

To add new LSP features (such as features new in LSP 3.15 or later) use these steps:

1. In protocol.lisp, add new server capability to |ServerCapabilities| interface.
   ex. `(|selectionRangeProvider| :optional t
                            :type (or boolean
                                      |SelectionRangeOptions|
                                      |SelectionRangeRegistrationOptions|))`
   Note: All LSP names are case-sensitive, so use "|" around names to specify case.

2. In protocol.lisp, add new client capability to either
   |TextDocumentClientCapabilities| or |ClientCapabilities| as appropriate.
   ex. `(define-interface |TextDocumentClientCapabilities|
        ...
        (|selectionRange| :optional t :type |SelectionRangeClientCapabilities|))`

3. In protocol.lisp, add any new interfaces that are specified.
   ex. `(define-interface |SelectionRange| ()
            (|range| :type |Range|)
            (|parent| :optional t :type |SelectionRange|))`

4. If these interfaces reference any other interfaces which are not defined (yet)
   then you need to add those here as well.
   ex. When adding SelectionRange capability, StaticRegistrationOptions capability
       had to be added as well.

5. In lsp-server.lsp, add any new methods associated with the new capability.
   ex. `(define-method "textDocument/selectionRange" (params) nil)`
