(defpackage :lsp-server/test
  (:use :gt/full
        :lsp-server
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server.lem-base
        :lsp-server/test-utils
        :lsp-server/ordered-dict
        :lsp-server/utilities/lsp-utilities
        :lsp-server/utilities/worker-thread
        :stefil+
        :software-evolution-library/utility/git)
  (:local-nicknames (:lp :lparallel))
  (:import-from :lsp-server :server-buffer-name :document)
  (:import-from :lsp-server/protocol
                :protocol-convert
                :json-map
                :skip-slot
                :lookup-lsp-path
                :*protocol-symbols*)
  (:import-from :lsp-server/spec
                :parse-property-path
                :parse-property-type
                :parse-method)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :with-temporary-server
           :with-temporary-buffer
           ;; Prevent complaints from SBCL.
           :|xtra| :|XtraClientCapabilities|))
(in-package :lsp-server/test)
(in-readtable :curry-compose-reader-macros)

(defroot test)

(defsuite lsp-server-tests "Test LSP server.")

(defun compare-variants (base base-copy variant1 variant2)
  (is (equal? base base))
  (is (equal? base-copy base-copy))
  (is (equal? variant1 variant1))
  (is (equal? variant2 variant2))
  (is (equal? base base-copy))
  (is (not (equal? base variant1)))
  (is (not (equal? base-copy variant1)))
  (is (not (equal? base variant2)))
  (is (not (equal? base-copy variant2)))
  (is (not (equal? variant1 variant2))))

(deftest test-protocol-equality ()
  (compare-variants
   (make '|Position| :|line| 1 :|character| 1)
   (make '|Position| :|line| 1 :|character| 1)
   (make '|Position| :|line| 1 :|character| 2)
   (make '|Position| :|line| 2 :|character| 1)))

(defun make-range (start-line start-col end-line end-col)
  (make-instance
   '|Range|
   :|start|
   (make-instance
    '|Position|
    :|line| start-line
    :|character| start-col)
   :|end|
   (make-instance
    '|Position|
    :|line| end-line
    :|character| end-col)))

(deftest test-nested-protocol-equality ()
  (compare-variants
   (make-range 1 1 1 5)
   (make-range 1 1 1 5)
   (make-range 1 1 2 5)
   (make-range 1 1 5 2)))

(deftest test-protocol-equality-with-inheritance ()
  (compare-variants
   (make '|VersionedTextDocumentIdentifier|
         :|version| 1
         :|uri| "file://foo")
   (make '|VersionedTextDocumentIdentifier|
         :|version| 1
         :|uri| "file://foo")
   (make '|VersionedTextDocumentIdentifier|
         :|version| 2
         :|uri| "file://foo")
   (make '|VersionedTextDocumentIdentifier|
         :|version| 2
         :|uri| "file://bar")))

(deftest test-convert ()
  (let ((id (make '|VersionedTextDocumentIdentifier|
                  :|uri| "file:///doc.txt"
                  :|version| 1)))
    (is (equal? id
                (convert '|TextDocumentIdentifier|
                         (convert 'hash-table id))))))

(deftest specific-string-required ()
  (signals error
           (convert '|WorkDoneProgressEnd|
                    (dict "kind" "done"
                          "message" "foo")))
  (let ((end (convert '|WorkDoneProgressEnd|
                      (dict "message" "foo"))))
    (is (equal "end" (gethash "kind" (convert 'hash-table end))))
    (is (equal? end
                (convert '|WorkDoneProgressEnd|
                         (convert 'hash-table
                                  end))))))

(deftest test-temporary-buffer ()
  (let ((text "Hello, world"))
    (with-temporary-buffer (b :text text)
      (is (equal text
                 (points-to-string
                  (buffer-start-point b)
                  (buffer-end-point b)))))))

(deftest test-serialize-diagnostics ()
  (is (equal "{\"context\":{\"diagnostics\":[]}}"
             (with-output-to-string (s)
               (yason:encode
                (make-instance '|CodeActionParams| :context
                               (make-instance '|CodeActionContext|
                                              :diagnostics nil))
                s)))))

(deftest test-protocol-convert ()
  (is (eql t (protocol-convert '(or boolean |SaveOptions|) t))))

(deftest test-document-sync-save-slot-type ()
  (finishes
   (convert '|TextDocumentSyncOptions|
            (dict "save" t))))

(deftest test-buffer-promise ()
  (unless (> (count-cpus :online t) 1)
    (format *error-output*
            "~&Skipping test because only 1 CPU is available.~%")
    (return-from test-buffer-promise))
  (loop repeat 10 do
    (let ((name (format nil "buffer-foo-~a" (random most-positive-fixnum)))
          b2)
      (unwind-protect
           (progn
             (is (null (get-buffer name)))
             (bt:make-thread
              (lambda ()
                (sleep (random 3))
                (setf b2 (get-buffer-create name))))
             (let* ((p (get-buffer-promise name))
                    (b (lp:force p)))
               (is (bufferp b))
               (is (equal name (buffer-name b)))
               (is (eql b (get-buffer name)))
               (is (eql b b2))))
        (delete-buffer (get-buffer name))
        (is (null (get-buffer name)))))))

(deftest test-client-caps-support ()
  (is (client-caps-support (make-instance '|ClientCapabilities|)
                           "textDocument/didOpen"))
  (is (client-caps-support (make-instance '|ClientCapabilities|)
                           "textDocument/didClose"))
  (is (client-caps-support (make-instance '|ClientCapabilities|)
                           "textDocument/didChange"))
  (is
   (client-caps-support
    (make-instance '|ClientCapabilities|
                   :workspace
                   (make-instance '|WorkspaceClientCapabilities|
                                  :did-change-configuration
                                  (make-instance '|DidChangeConfigurationClientCapabilities|)))
    "workspace/didChangeConfiguration")))

(deftest test-server-caps-support ()
  (is (server-caps-support (make-instance '|ServerCapabilities|)
                           "$/cancel"))
  (is (server-caps-support (make-instance '|ServerCapabilities|)
                           "$/progress")))

(deftest test-server-caps-support-optional ()
  (is (null (server-caps-support
             (make-instance '|ServerCapabilities|)
             "workspace/willCreateFiles")))
  (is (null (server-caps-support
             (make-instance '|ServerCapabilities|)
             "textDocument/willSave")))
  (is (null (server-caps-support
             (make-instance '|ServerCapabilities|
                            :text-document-sync
                            (make-instance '|TextDocumentSyncOptions|
                                           :will-save nil))
             "textDocument/willSave")))
  (is (server-caps-support
       (make-instance '|ServerCapabilities|
                      :text-document-sync
                      (make-instance '|TextDocumentSyncOptions|
                                     :will-save t))
       "textDocument/willSave")))

(deftest test-parse-property-path ()
  (is (equal (list "general.regularExpressions" nil)
             (parse-property-path
              "* property path: `general.regularExpressions`")))
  (is (equal (list "general.regularExpressions" " (optional)")
             (parse-property-path
              "* property path (optional): `general.regularExpressions`")))

  (is (equal
       "workspace.fileOperations.willCreate"
       (first
        (parse-property-path
         "* property name (optional): `workspace.fileOperations.willCreate`")))))

(deftest test-parse-property-type ()
  (is (equal "RegularExpressionsClientCapabilities"
             (parse-property-type
              "* property type: `RegularExpressionsClientCapabilities` defined as follows:")))

  (is (equal "boolean"
             (parse-property-type
              "* property type: `boolean`")))

  (is (equal "boolean | WorkspaceSymbolOptions"
             (parse-property-type
              "* property type: `boolean | WorkspaceSymbolOptions` where `WorkspaceSymbolOptions` is defined as follows:")))

  (is (equal
       "boolean | ImplementationOptions | ImplementationRegistrationOptions"
       (parse-property-type
        "* property type: `boolean | ImplementationOptions | ImplementationRegistrationOptions` where `ImplementationOptions` is defined as follows:
")))

  (is (equal
       "PublishDiagnosticsClientCapabilities"
       (parse-property-type
        "* property type `PublishDiagnosticsClientCapabilities` defined as follows:"))))

(deftest test-parse-method ()
  (is (equal "textDocument/didSave"
             (parse-method
              "* method: 'textDocument/didSave'")))
  (is (equal
       "textDocument/linkedEditingRange"
       (parse-method
        "* method: `textDocument/linkedEditingRange`"))))

(deftest test-convert-json-map ()
  (let* ((in
          (dict "file://foo"
                (list
                 (dict "range"
                       (dict "start" (dict "line" 0 "character" 0)
                             "end" (dict "line" 0 "character" 0))
                       "newText" "bar"))))
         (out (protocol-convert
               '(json-map (uri |DocumentUri|)
                 (trivial-types:proper-list |TextEdit|))
               in)))
    (is (every (op (every (of-type '|TextEdit|) _))
               (hash-table-values out))))
  (let* ((in
          (dict "file://foo"
                (list
                 (dict "range"
                       (dict "start" (dict "line" 0 "character" 0)
                             "end" (dict "line" 0 "character" 0))
                       "newText" "bar"))))
         (out (protocol-convert
               '(json-map (uri |DocumentUri|)
                 (serapeum:soft-list-of |TextEdit|))
               in)))
    (is (every (op (every (of-type '|TextEdit|) _))
               (hash-table-values out)))))

(define-interface |XtraClientCapabilities| (|ClientCapabilities|)
  (|xtra| :type boolean))

(deftest test-convert-extra-args ()
  (let ((caps (convert '|ClientCapabilities| (dict "xtra" t))))
    (is (eql t (slot-value
                (change-class caps '|XtraClientCapabilities|)
                '|xtra|)))))

(deftest test-parse-annotated-edit ()
  (let* ((data (dict "textDocument"
                     (dict "version" nil
                           "uri" "file://none")
                     "edits"
                     (list (dict "annotationId" "foo"
                                 "range"
                                 (dict "start" (dict "line" 5 "character" 23)
                                       "end" (dict "line" 6 "character" 0))
                                 "newText" "scribble"))))
         (lsp (convert '|TextDocumentEdit| data))
         (edit (first (slot-value lsp '|edits|))))
    (is (typep edit '|AnnotatedTextEdit|))))

(deftest test-ordered-dict ()
  (let ((dict (ordered-dict "timmy" "red"
                            "barry" "green"
                            "guido" "blue")))
    (is (= 3 (size dict)))
    (is (equal '(("timmy" . "red")
                 ("barry" . "green")
                 ("guido" . "blue"))
               (convert 'alist dict)))
    (is (equal (lookup dict "timmy") "red"))
    (is (equal (lookup dict "barry") "green"))
    (is (equal (lookup dict "guido") "blue"))
    (is (equal (lookup dict "john") nil))

    (delitem dict "guido")
    (is (= (size dict) 2))
    (is (equal (lookup dict "guido") nil))

    (signals error
             (popitem (ordered-dict)))
    (is (null (delitem (ordered-dict) "guido")))))

(deftest test-convert-slot-regressions ()
  (is (null (protocol-convert 'boolean 'yason:false)))
  (is (null (protocol-convert '(or boolean |SaveOptions|)
                              'yason:false)))
  (is (typep
       (protocol-convert '(or boolean |ReferenceOptions|)
                         (dictq "workDoneProgress" yason:false))
       '|ReferenceOptions|)))

(deftest test-null-for-optional-slot ()
  (handler-bind ((type-error
                  (lambda (e)
                    (when (typep (type-error-datum e) 'null)
                      (invoke-restart 'skip-slot)))))
    (is (typep
         (protocol-convert '|ServerCapabilities|
                           (dict "completionProvider" nil))
         '|ServerCapabilities|))))

(deftest test-lookup-lsp-path ()
  (is (null
       (lookup-lsp-path
        (make-instance '|ServerCapabilities|)
        '(|completionProvider|)
        t)))
  (is (true
       (lookup-lsp-path
        (make-instance '|ServerCapabilities|
                       :|completionProvider|
                       (make '|CompletionOptions|))
        '(|completionProvider|)
        t))))

(deftest test-initialize-params-safe ()
  (finishes
   (is (null (initialize-params-safe)))))

(deftest test-client-info-name ()
  (is (null (client-info-name :params nil)))
  (is (stringp
       (client-info-name :params
                         (convert '|InitializeParams|
                                  (dict "processId" nil
                                        "clientInfo" (dict "name" "myEditor")
                                        "rootUri" nil
                                        "capabilities" (dict)))))))

(deftest test-name-protocol-symbol ()
  (do-hash-table (k v *protocol-symbols*)
    (declare (ignore v))
    (is (eql (name-protocol-symbol (string k))
             k))))

(deftest test-enforce-slot-types ()
  (signals type-error
    (make '|VersionedTextDocumentIdentifier|
          :uri "http://example.com"
          :version nil))
  (signals type-error
    (make '|WorkspaceEdit|
          :document-changes
          (make '|TextDocumentEdit|
                :text-document
                (make '|OptionalVersionedTextDocumentIdentifier|
                      :uri "http://example.com")
                :edits ()))))

(defclass my-position (|Position|)
  ())

(deftest test-interface-name ()
  (is (eql (interface-name "") nil))
  (is (eql '|Position|
           (interface-name (make 'my-position :line 0 :character 0)))))

(deftest test-convert-command-without-arguments-to-hash-table ()
  (is (hash-table-p
       (convert 'hash-table
                (make '|Command|
                      :title "Title"
                      :command "command")))))

(deftest test-file-ops ()
  (let ((types '(|CreateFile| |RenameFile| |DeleteFile|))
        (dicts
         (list (dict "kind" "create" "uri" "file://uri")
               (dict "kind" "rename"
                     "oldUri" "file://olduri"
                     "newUri" "file://newuri")
               (dict "kind" "delete"
                     "uri" "file://uri"))))
    (iter (for type in types)
          (for dict in dicts)
          (is (typep (protocol-convert `(or ,@(shuffle (copy-list types)))
                                       dict)
                     type)))))

(deftest test-copy-protocol ()
  (let ((command
         (make '|Command| :title "Human Readable" :command "for-computers")))
    (let ((copy (copy-protocol command)))
      (is (equal? copy command)))
    (let ((copy (copy-protocol command :title "More Readable")))
      (is (not (equal? copy command)))
      (is (equal "More Readable" (slot-value copy '|title|)))
      (is (equal "for-computers" (slot-value copy '|command|))))
    (let ((copy (copy-protocol command :arguments '(1 2 3))))
      (is (not (equal? copy command)))
      (is (equal "Human Readable" (slot-value copy '|title|)))
      (is (equal "for-computers" (slot-value copy '|command|)))
      (is (equal '(1 2 3) (slot-value copy '|arguments|))))))

(deftest test-copy-protocol-with-inherited-slots ()
  (let ((id (make '|VersionedTextDocumentIdentifier|
                  :version 1
                  :uri "file://my-file")))
    (is (typep
         (copy id :version 2)
         '|VersionedTextDocumentIdentifier|))))

(deftest test-file-uri-path ()
  (is (equal (file-uri-path "file:///tmp/t.c") "/tmp/t.c"))
  (is (equal (file-uri-path "file:///My%20file") "/My file")))

(deftest test-file-path-uri ()
  (is (equal (file-path-uri "/tmp/t.c") "file:///tmp/t.c"))
  (is (equal (file-path-uri "/My file") "file:///My+file")))

(deftest test-path-from-params ()
  (is (equal (pathname "/tmp/t.c")
             (nest (path-from-params)
                   (make '|DidOpenTextDocumentParams| :text-document)
                   (make '|TextDocumentItem|
                         :uri "file:///tmp/t.c"
                         :language-id ""
                         :version 0
                         :text "")))))

(deftest test-make-position ()
  (is (equalp (nest (convert 'hash-table)
                    (make '|Position| :line 10 :character 1))
              (nest (convert 'hash-table)
                    (make-position 10))))
  (is (equalp (nest (convert 'hash-table)
                    (make '|Position| :line 10 :character 10))
              (nest (convert 'hash-table)
                    (make-position 10 10)))))

(deftest test-make-empty-range ()
  (is (equalp (nest (convert 'hash-table)
                    (make '|Range|
                          :start (make '|Position| :line 1 :character 1)
                          :end (make '|Position| :line 1 :character 1)))
              (nest (convert 'hash-table)
                    (make-empty-range)
                    (make '|Position| :line 1 :character 1)))))

(deftest test-make-lsp-worker-thread ()
  (let* ((counter 0)
         (thread (make-lsp-worker-thread (lambda () (incf counter)))))
    (join-thread thread)
    (is (equal counter 1))))
