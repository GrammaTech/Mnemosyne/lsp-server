(uiop:define-package :lsp-server/protocol
  (:use :cl)
  (:import-from :gt
                :parse-leading-keywords
                :length=
                :assure
                :match
                :ematch
                :string-join
                :lines
                :find-readtable
                :mvlet
                :find-keyword
                :ellipsize
                :defconst
                :equal?
                :synchronized
                :eval-always
                :read-file-into-string
                :export-always
                :string+
                :lastcar
                :compose
                :make-keyword
                :mapply
                :nest
                :split-sequence
                :nlet
                :->
                :string-designator
                :slot-value-safe
                :with-unique-names
                :soft-list-of
                :mvlet*)
  (:import-from :serapeum :range)
  (:import-from :trivial-types
                :proper-list)
  (:import-from :fset :convert)
  (:import-from :jsonrpc :jsonrpc-error)
  (:import-from :lsp-server/spec
                :cap-optional?
                :cap-path
                :validate-lsp-symbols
                :*client-caps*
                :*server-caps*)
  (:import-from :alexandria
                :when-let
                :ignore-some-conditions)
  (:shadow :required-argument)
  (:export
    :|uinteger|
    :convert-from-hash-table
    :convert-to-hash-table
    :protocol-symbol-p
    :protocol
    :copy-protocol
    :define-interface
    :define-namespaced-constants
    :request-cancelled
    :content-modified
    :client-caps-support
    :server-caps-support
    :lookup-lsp-path
    :|ChangeAnnotationIdentifier|
    :protocol-hash-table
    :name-protocol-symbol
    :interface-name))
(in-package :lsp-server/protocol)


;;; Infrastructure

(define-condition required-argument (error)
  ((name :initarg :name))
  (:report (lambda (c s)
             (with-slots (name) c
               (format s "Required argument ~@[~S ~]missing." name)))))

(defun required-argument (&optional name)
  "Signals an error for a missing argument of NAME. Intended for
use as an initialization form for structure and class-slots, and
a default value for required keyword arguments."
  (error 'required-argument :name name))

(deftype json-map (&optional (key t) (value t))
  "A JSON map (a hash table)."
  (declare (ignore key value))
  'hash-table)

(defmethod convert ((type (eql 'boolean))
                    (obj (eql 'yason:false))
                    &key)
  nil)

(defmethod convert ((type (eql 'boolean))
                    (obj (eql 'yason:true))
                    &key)
  t)

(defvar *protocol-symbols*
  (make-hash-table :size 255)
  "Symbols that are the names of protocol classes.")

(defvar *protocol-symbol-names*
  (make-hash-table :size 255 :test 'equal)
  "Table from names to protocol symbols.")

(-> name-protocol-symbol (string-designator &key (:error t)) symbol)
(defun name-protocol-symbol (name &key error)
  "Get the symbol for the protocol named NAME."
  (or (values (gethash (string name) *protocol-symbol-names*))
      (and error
           (error "No protocol symbol named ~a" name))))

(defclass protocol ()
  ((hash-table
    :initarg :hash-table
    :reader protocol-hash-table
    :documentation "The hash table used to construct this instance, if
    there was one."))
  (:documentation "Base class for LSP protocol interfaces."))

(defmethod print-object ((self protocol) stream)
  (print-unreadable-object (self stream :type t)
    (let ((s (with-output-to-string (s)
               (yason:encode self s))))
      (write-string (ellipsize s 64) stream))))

(defmethod convert ((to (eql 'protocol)) (x protocol) &key)
  x)

(defmethod convert ((to (eql 'hash-table)) (x protocol) &key)
  (convert-to-hash-table x))
(defmethod convert ((to (eql 'hash-table)) (x hash-table) &key)
  x)

(defgeneric copy-protocol (x &key)
  (:documentation "Copy protocol object X.
Can pass keywords to override individual slots."))

(defgeneric copy-protocol-args (x &key &allow-other-keys)
  (:documentation "Copy protocol object X.
Can pass keywords to override individual slots.")
  (:method (x &key) nil))

(defmethod gt:copy ((obj protocol) &rest args &key)
  (apply #'copy-protocol obj args))

(defgeneric convert-to-hash-table (instance)
  (:documentation "Convert a protocol instance to a hash table.

More generally, notwithstanding the name, convert an object into a
form that can be serialized to JSON (not necessarily a hash table).")
  (:method ((object string)) object)
  (:method ((object number)) object)
  (:method ((object symbol))
    (if (keywordp object) (string object)
        (call-next-method)))
  (:method ((instance hash-table))
    instance)
  (:method ((instance protocol))
    (convert-to-hash-table* instance (slot-specs/cached instance))))

(defgeneric convert-from-hash-table (name hash-table)
  (:documentation "Create a protocol instance from a hash table.")
  (:method ((name symbol) (hash-table hash-table))
    (convert-from-hash-table
     ;; A hack to work around the lack of
     ;; class methods.
     (allocate-instance (find-class name))
     hash-table))
  (:method ((instance protocol) (hash-table hash-table))
    (convert-from-hash-table* (class-name (class-of instance))
                              hash-table
                              (slot-specs/cached instance))))

(defgeneric slot-specs (instance)
  (:method-combination append)
  (:documentation "Get an alist of (slot-name . declared-type) for the slots of INSTANCE."))

(let ((cache (make-hash-table)))
  (defun slot-specs/cached (instance)
    "Memoized wrapper for `slot-specs'."
    (let ((class-name (class-name (class-of instance))))
      (multiple-value-bind (cached cached?)
          (gethash class-name cache)
        (if cached? cached
            (synchronized (cache)
              (setf (gethash class-name cache)
                    (slot-specs instance)))))))
  (defun uncache-slot-specs (class-name)
    (remhash class-name cache)))

(defgeneric protocol-equal? (x y)
  (:method-combination and)
  (:documentation "Check that two protocol objects are equal."))

(defmethod equal? ((x protocol) (y protocol))
  (protocol-equal? x y))

(eval-always
 (defun format-lsp-type (type)
   "Format TYPE as if it were a TypeScript type."
   (assure string
     (ematch type
       ((eql t) (format-lsp-type '|any|))
       ((list 'eql (and value (type keyword)))
        (format nil "'~a'" value))
       ((list (or 'proper-list 'soft-list-of) (and (list* 'or _) element-type))
        (format nil "(~a)[]" (format-lsp-type element-type)))
       ((list (or 'proper-list 'soft-list-of) element-type)
        (format nil "~a[]" (format-lsp-type element-type)))
       ;; This is the syntax that the LSP spec uses.
       ((list 'json-map (list key-name from-type) to-type)
        (format nil "{ [~a: ~a]: ~a }"
                key-name
                (format-lsp-type from-type)
                (format-lsp-type to-type)))
       ((list* 'or alternatives)
        (format nil "~{~a~^ | ~}"
                (mapcar #'format-lsp-type alternatives)))
       ((list* 'and alternatives)
        (format nil "~{~a~^ & ~}"
                (mapcar #'format-lsp-type alternatives)))
       ((eql 'list) "any[]")
       ((or (list 'integer 0 '*)
            (list 'integer 0)
            (eql '|uinteger|))
        "uinteger")
       ((and type (type symbol))
        (if (every #'upper-case-p (string type))
            (string-downcase type)
            (string type))))))

 (defun format-as-lsp-interface (name supers slots
                                 &aux (*readtable* (find-readtable :standard)))
   "Format a class definition as if it were a TypeScript interface."
   (format nil
           #.(string-join
              '("An LSP interface definition.~%"
                ;; Print the name of the interface, and the
                ;; superclasses, if any.
                "export interface ~a~@[ extends ~{~a~^,~}~] {"
                ;; Iterate over the slots, as (docstring slot-name optional? type).
                "~:{~"
                ;; Output a docstring, if there is one, in jsdoc
                ;; format.
                "~&~@[        /**"
                ;; Format the docstring. The docstring is expected to
                ;; have already been split into lines.
                "~{         * ~a~^~%~}"
                ;; Terminate the docstring.
                "         */~%~]~"
                ;; Output the slot, whether it is required, and its
                ;; type, with a newline afterward.
                "~&        ~a~:[~;?~]: ~a;~%~"
                ;; Terminate processing the slots.
                "~}~"
                ;; Print the closing brace.
                "~&}")
              #\Newline)
           name supers
           (mapcar (lambda (spec)
                     (destructuring-bind (name &key
                                                 optional
                                                 documentation
                                                 (type t)
                                          &allow-other-keys)
                         spec
                       (list (and documentation (lines documentation))
                             name
                             optional
                             (format-lsp-type type))))
                   slots)))

 (defun format-as-lsp-namespace (ns names values)
   (assert (length= names values))
   (let ((*readtable* (find-readtable :standard)))
     (format nil "An LSP namespace.~2%export namespace ~a {~%~:{~4texport const ~a = ~a;~%~}~&}"
             ns
             (mapcar #'list names
                     (loop for value in values
                           if (symbolp value)
                             collect (string+ "'" value "'")
                           else collect value)))))

 (defun lispy-initarg (symbol)
   "Return a Lispy initarg for SYMBOL.
If SYMBOL is all-lowercase, just upcase it.

If SYMBOL is mixed-case, put a hyphen before each uppercase
character (unless it is the first) and upcase the result.

    newText -> :NEW-TEXT
    didChangeConfiguration -> :DID-CHANGE-CONFIGURATION"
   (let ((string (string symbol)))
     (if (notany #'upper-case-p string)
         (make-keyword (string-upcase string))
         (let ((words
                (gt:runs string :key #'upper-case-p)))
           (make-keyword
            (string-upcase
             (gt:string-join
              (cons (first words)
                    (gt:mappend (lambda (word)
                                  (if (every #'upper-case-p word)
                                      (list "-" word)
                                      (list word)))
                                (rest words)))
              ""))))))))

(defvar *lsp-classes*
  (make-hash-table :test 'equal)
  "Table from (string) name to (symbol) class name.")

(define-condition slot-type-error (type-error)
  ((slot :initarg :slot :type symbol :reader slot-type-error-slot)
   (class :initarg :class :type symbol :reader slot-type-error-class))
  (:report (lambda (c s)
             (with-accessors ((slot slot-type-error-slot)
                              (class slot-type-error-class)
                              (datum type-error-datum)
                              (type type-error-expected-type))
                 c
               (format s "Slot ~a in class ~a expects type ~a, got:~%~a"
                       slot class type datum)))))

(declaim (notinline signal-type-error))
(defun signal-type-error (slot class datum type)
  "Signal a type error (out of line)."
  (error 'slot-type-error
         :class class
         :slot slot
         :datum datum
         :expected-type type))

(defgeneric interface-name (class)
  (:documentation "The name of the interface class of CLASS. This
allows retrieving the original class name when subclassing protocol
classes (for convenience).")
  (:method ((x t)) nil))

(defmacro define-interface (name parent &body options-and-slots)
  "Define an LSP interface.
LSP interface names are statically validated against the LSP spec at
compile time. To skip static validation (e.g. if you want to define a
name for an implicit nested protocol object), specify
`:static-validation nil` before defining the slots."
  (mvlet* ((options slots
            (parse-leading-keywords options-and-slots))
           (slot-specs
            (mapply
             (lambda (slot-symbol &key (type t) optional documentation initform)
               (declare (ignore documentation optional initform))
               (cons slot-symbol type))
             slots)))
    (let ((syms (cons name (mapcar #'first slots))))
      (assert (notany (lambda (sym)
                        (every #'upper-case-p (string sym)))
                      (cons name syms)))
      (when (eql (symbol-package name)
                 (find-package :lsp-server/protocol))
        (when (getf options :static-validate t)
          (validate-lsp-symbols syms))))
    `(progn
       (uncache-slot-specs ',name)
       (setf (gethash ',name *protocol-symbols*) ',name
             (gethash ,(string name) *protocol-symbol-names*) ',name)
       (export ',(cons name (mapcar #'first slots)))
       (defclass ,name ,(if (null parent)
                            `(protocol)
                            parent)
         ,(mapply
           (lambda (slot-symbol
                    &key type optional documentation
                      (initform nil initform-supplied?))
             `(,slot-symbol
               ;; A case-sensitive initarg, for consistency.
               :initarg ,(intern (string slot-symbol) :keyword)
               ;; A Lispy initarg, for convenience.
               :initarg ,(lispy-initarg slot-symbol)
               :type ,(or type t)
               ,@(and (not optional)
                      (not initform-supplied?)
                      `(:initform
                        (required-argument ',slot-symbol)))
               ,@(and initform-supplied?
                      `(:initform ,initform))
               ,@(if documentation
                     `(:documentation ,documentation))))
           slots)
         (:documentation
          ,(format-as-lsp-interface name parent slots)))
       (defmethod copy-protocol
           ((x ,name) &rest args &key &allow-other-keys)
         (apply #'make-instance ',name
                (apply #'copy-protocol-args x args)))
       ,(let* ((x (gensym))
               (slot-names (mapcar #'car slots))
               (supplied-p-vars
                (loop for slot-name in slot-names
                      collect (gensym (string+ slot-name '-supplied-p)))))
          `(defmethod copy-protocol-args
               ((,x ,name)
                &key
                  ,@(mapcar (lambda (slot-name supplied-p)
                              `((,(lispy-initarg slot-name)
                                 ,slot-name)
                                nil
                                ,supplied-p))
                            slot-names
                            supplied-p-vars))
             (append
              ,@(mapcar
                 (lambda (slot supplied-p)
                   (destructuring-bind (slot-name &key optional
                                        &allow-other-keys)
                       slot
                     (let ((initarg (lispy-initarg slot-name)))
                       (if optional
                           `(if ,supplied-p
                                (list ,initarg ,slot-name)
                                (when (slot-boundp ,x ',slot-name)
                                  (list ,initarg
                                        (slot-value ,x ',slot-name))))
                           `(list
                             ,initarg
                             (if ,supplied-p
                                 ,slot-name
                                 (slot-value ,x ',slot-name)))))))
                 slots
                 supplied-p-vars)
              (call-next-method))))
       (defmethod interface-name ((x ,name))
         ',name)
       (defmethod slot-specs append ((instance ,name))
         ',slot-specs)
       (defmethod protocol-equal? and ((x ,name) (y ,name))
         (and ,@(loop for (sym nil) in slot-specs
                      for temp = (gensym)
                      collect `(let ((,temp (slot-boundp x ',sym)))
                                 (and (eql ,temp (slot-boundp y ',sym))
                                      (if ,temp
                                          (equal? (slot-value x ',sym)
                                                  (slot-value y ',sym))
                                          t))))))
       (defmethod convert ((name (eql ',name))
                           (table hash-table)
                           &key)
         (convert-from-hash-table name table))
       ;; Converting to self should be idempotent.
       (defmethod convert ((name (eql ',name))
                           (obj ,name)
                           &key)
         obj)
       ,@(when slot-specs
           (with-unique-names (self slot-names)
             `((defmethod shared-initialize :after ((,self ,name) ,slot-names
                                                    &key)
                 (declare (ignore ,slot-names))
                 (with-slots ,(mapcar #'car slot-specs) ,self
                   ,@(loop for (slot . type) in slot-specs
                           collect `(when (slot-boundp ,self ',slot)
                                      (unless (typep ,slot ',type)
                                        (signal-type-error
                                         ',slot ',name ,slot ',type)))))))))
       ',name)))

(defmacro define-namespaced-constants (ns (&key) &body constants)
  "Define an LSP-style namespace of constants."
  (multiple-value-bind (names values)
      (loop for (name value) in constants
            collect name into names
            collect value into values
            finally (return (values names values)))
    (when (eql (symbol-package ns)
               (find-package :lsp-server/protocol))
      (validate-lsp-symbols (cons ns names)))
    ;; Sanity check: are all the values unique?
    (assert (equal values (remove-duplicates values)))
    (let* ((prefixed-names
            (loop for name in names
                  collect (intern (string+ ns "." name)
                                  (symbol-package ns))))
           (type
            (if (and (every #'integerp values)
                     (every #'< values (rest values))
                     (equal? values
                             (range (first values)
                                    (1+ (lastcar values)))))
                `(integer ,(first values)
                          ,(lastcar values))
                `(member ,@values)))
           (docstring
            ;; Bind readtable to control how symbols are printed.
            (format-as-lsp-namespace ns names values)))
      `(progn
         (deftype ,ns () ,docstring ',type)
         ,@(loop for name in prefixed-names
                 for value in values
                 collect `(defconst ,name ,value
                            ,(format nil "A constant in the Language Server Protocol namespace ~a (value ~a)." ns value)))
         ,@(when (loop for value in values
                       always (keywordp value))
             `((defmethod convert ((type (eql ',ns))
                                   value
                                   &key)
                 (find-keyword value))))
         (export-always '(,ns ,@prefixed-names))))))

(define-condition request-cancelled (jsonrpc-error)
  ()
  (:default-initargs
   :code -32800
   :message "Request Cancelled"))

(define-condition content-modified (jsonrpc-error)
  ()
  (:default-initargs
   :code -32801
   :message "Content Modified"))


;;; LSP definitions

(deftype |uinteger| ()
  "Unsigned integer."
  '(integer 0 *))

(deftype |DocumentUri| ()
  "A URI for a document."
  'string)
(export '(|DocumentUri|))

(deftype uri ()
  "A normal, non-document URI."
  'string)

(defmethod convert ((type (eql '|DocumentUri|)) (s string) &key)
  s)

(define-interface |Position| ()
  (|line| :type (integer 0 *) :documentation "Zero-based line number.")
  (|character|
   :type (integer 0 *)
   :documentation "Zero-based character offset (assuming UTF-16!)."))

(define-interface |Range| ()
  (|start| :type |Position|)
  (|end| :type |Position|))

(define-interface |Location| ()
  (|uri| :type |DocumentUri|)
  (|range| :type |Range|))

(define-interface |Diagnostic| ()
  (|range| :type |Range|)
  (|severity| :optional t :type (or null number))
  (|code| :optional t :type (or null number string))
  (|source| :optional t :type (or null string))
  (|message| :type string))

(define-namespaced-constants |DiagnosticSeverity| ()
  (|Error| 1)
  (|Warning| 2)
  (|Information| 3)
  (|Hint| 4))

(define-interface |Command| ()
  (|title| :type string)
  (|command| :type string)
  (|arguments| :optional t :type list))

(define-interface |TextEdit| ()
  (|range| :type |Range|)
  (|newText| :type string))

(define-interface |ChangeAnnotation| ()
  (|label| :type string)
  (|needsConfirmation| :type boolean :optional t)
  (|description| :type string :optional t))

(deftype |ChangeAnnotationIdentifier| () 'string)

(define-interface |AnnotatedTextEdit| (|TextEdit|)
  (|annotationId| :type |ChangeAnnotationIdentifier|))

(define-interface |TextDocumentEdit| ()
  (|textDocument| :type |OptionalVersionedTextDocumentIdentifier|)
  (|edits| :type (proper-list (or |AnnotatedTextEdit| |TextEdit|))))

(define-interface |CreateFileOptions| ()
  (|overwrite| :optional t :type boolean)
  (|ignoreIfExists| :optional t :type boolean))

(define-interface |CreateFile| ()
  (|kind| :type (eql :|create|) :initform :|create|)
  (|uri| :type |DocumentUri|)
  (|options| :optional t :type |CreateFileOptions|)
  (|annotationId| :optional t :type |ChangeAnnotationIdentifier|))

(define-interface |RenameFileOptions| ()
  (|overwrite| :optional t :type boolean)
  (|ignoreIfExists| :optional t :type boolean))

(define-interface |RenameFile| ()
  (|kind| :type (eql :|rename|) :initform :|rename|)
  (|oldUri| :type |DocumentUri|)
  (|newUri| :type |DocumentUri|)
  (|options| :optional t :type |RenameFileOptions|)
  (|annotationId| :optional t :type |ChangeAnnotationIdentifier|))

(define-interface |DeleteFileOptions| ()
  (|recursive| :optional t :type boolean)
  (|ignoreIfNotExists| :optional t :type boolean))

(define-interface |DeleteFile| ()
  (|kind| :type (eql :|delete|) :initform :|delete|)
  (|uri| :type |DocumentUri|)
  (|options| :optional t :type |DeleteFileOptions|)
  (|annotationId| :optional t :type |ChangeAnnotationIdentifier|))

(define-interface |WorkspaceEdit| ()
  (|changes|
   :optional t
   :type (json-map (uri |DocumentUri|) (proper-list |TextEdit|)))
  (|documentChanges|
   :optional t
   :type (proper-list (or |TextDocumentEdit|
                          |CreateFile|
                          |RenameFile|
                          |DeleteFile|)))
  ;; Table from ChangeAnnotationID to
  (|changeAnnotations|
   :optional t
   :type (json-map (|id| |ChangeAnnotationIdentifier|) |ChangeAnnotation|)))

(define-interface |TextDocumentIdentifier| ()
  (|uri| :type |DocumentUri|))

(define-interface |TextDocumentItem| ()
  (|uri| :type |DocumentUri|)
  (|languageId| :type string)
  (|version| :type number)
  (|text| :type string))

(define-interface |VersionedTextDocumentIdentifier|
    (|TextDocumentIdentifier|)
  (|version| :type integer))

(define-interface |OptionalVersionedTextDocumentIdentifier|
    (|TextDocumentIdentifier|)
  (|version| :type (or null integer) :initform nil))

(define-interface |TextDocumentPositionParams| ()
  (|textDocument| :type |TextDocumentIdentifier|)
  (|position| :type |Position|))

(define-interface |DocumentFilter| ()
  (|language| :optional t :type string)
  (|scheme| :optional t :type string)
  (|pattern| :optional t :type string))

(deftype |DocumentSelector| ()
  '(proper-list |DocumentFilter|))

;;; NB Not defined in LSP.
(define-interface |clientInfo| ()
  (|name| :type string)
  (|version| :type string :optional t))

(define-interface |InitializeParams| ()
  (|clientInfo| :type |clientInfo| :optional t)
  (|processId| :type (or number null))
  (|rootPath| :type (or string null) :optional t)
  (|rootUri| :type (or |DocumentUri| null))
  (|workspaceFolders|
   :type (or (soft-list-of |WorkspaceFolder|) null)
   :optional t)
  (|initializationOptions| :optional t)
  (|capabilities| :type |ClientCapabilities|)
  (|trace| :optional t))

(define-interface |InitializedParams| ()
  ;; No slots.
  )

(deftype |ResourceOperationKind| () 'string)

(deftype |FailureHandlingKind| () 'string)

(define-interface |WorkspaceEditClientCapabilities| ()
  (|documentChanges| :optional t :type boolean)
  ;; Client supports 'create', 'rename', and 'delete' files and folders
  (|resourceOperations| :optional t :type (proper-list |ResourceOperationKind|))
  ;; Failure handling if a workspace edit fails.
  (|failureHandling| :optional t :type |FailureHandlingKind|)
  (|normalizesLineEndings| :optional t :type boolean)
  (|changeAnnotationSupport| :optional t :type t))

(define-interface |ExecuteCommandClientCapabilities| ()
  (|dynamicRegistration| :optional t :type boolean))

(define-interface |DidChangeConfigurationClientCapabilities| ()
  (|dynamicRegistration| :optional t :type boolean))

(define-interface |CodeLensWorkspaceClientCapabilities| ()
  (|refreshSupport| :optional t :type boolean))

;;; NB This is not part of the spec per se; this abstracts the
;;; optional workspace slot on ClientCapabilities.
(define-interface |WorkspaceClientCapabilities| ()
  (|applyEdit| :optional t :type boolean)
  (|workspaceEdit| :optional t :type |WorkspaceEditClientCapabilities|)
  (|didChangeConfiguration|
   :optional t
   :type |DidChangeConfigurationClientCapabilities|)
  (|didChangeWatchedFiles| :optional t)
  (|symbol| :optional t)
  (|executeCommand| :optional t :type |ExecuteCommandClientCapabilities|)
  (|workspaceFolders| :optional t :type boolean)
  (|configuration| :optional t :type boolean)
  (|codeLens| :optional t :type |CodeLensWorkspaceClientCapabilities|))

(define-interface |WindowClientCapabilities| ()
  (|workDoneProgress| :optional t :type boolean)
  (|showDocument| :optional t :type |ShowDocumentClientCapabilities|))

(define-interface |ShowDocumentClientCapabilities| ()
  (|support| :type boolean))

(define-interface |TextDocumentClientCapabilities| ()
  (|synchronization| :optional t)
  (|completion| :optional t)
  (|hover| :optional t)
  (|signatureHelp| :optional t)
  (|references| :optional t :type |ReferenceClientCapabilities|)
  (|documentHighlight| :optional t :type |DocumentHighlightClientCapabilities|)
  (|documentSymbol| :optional t)
  (|formatting| :optional t)
  (|rangeFormatting| :optional t)
  (|onTypeFormatting| :optional t)
  (|definition| :optional t :type |DefinitionClientCapabilities|)
  (|codeAction| :optional t)
  (|codeLens| :optional t)
  (|documentLink| :optional t)
  (|rename| :optional t)
  (|selectionRange| :optional t :type |SelectionRangeClientCapabilities|))

;;; Not part of LSP.
(define-interface |additionalPropertiesSupport| ()
  (|additionalPropertiesSupport| :type boolean :optional t))

(define-interface |ShowMessageRequestClientCapabilities| ()
  (|messageActionItem| :optional t :type |additionalPropertiesSupport|))

(define-interface |ClientCapabilities| ()
  (|showMessage| :optional t :type |ShowMessageRequestClientCapabilities|)
  (|workspace| :optional t :type |WorkspaceClientCapabilities|)
  (|textDocument| :optional t :type |TextDocumentClientCapabilities|)
  (|window| :optional t :type |WindowClientCapabilities|)
  (|experimental| :optional t :type t))

(define-interface |InitializeResult| ()
  (|capabilities| :type |ServerCapabilities|))

(define-interface |InitializeError| ()
  (|retry| :type boolean))

(define-namespaced-constants |TextDocumentSyncKind| ()
  (|None| 0)
  (|Full| 1)
  (|Incremental| 2))

(define-interface |CompletionOptions| ()
  (|resolveProvider| :optional t :type boolean)
  (|triggerCharacters| :optional t :type (proper-list string)))

(define-interface |SignatureHelpOptions| ()
  (|triggerCharacters| :optional t :type (proper-list string))
  (|retriggerCharacters| :optional t :type (proper-list string)))

(define-interface |CodeLensOptions| (|WorkDoneProgressOptions|)
  (|resolveProvider| :optional t :type boolean))

(define-interface |CodeLensRegistrationOptions| (|TextDocumentRegistrationOptions| |CodeLensOptions|))

(define-interface |DocumentOnTypeFormattingOptions| ()
  (|firstTriggerCharacter| :type string)
  (|moreTriggerCharacter| :optional t :type (proper-list string)))

(define-interface |DocumentLinkOptions| ()
  (|resolveProvider| :optional t :type boolean))

(define-interface |ExecuteCommandOptions| ()
  (|commands| :type (proper-list string)))

(define-interface |SaveOptions| ()
  (|includeText| :optional t :type boolean))

(define-interface |TextDocumentSyncOptions| ()
  (|openClose| :optional t :type boolean)
  (|change| :optional t :type |TextDocumentSyncKind|)
  (|willSave| :optional t :type boolean)
  (|willSaveWaitUntil| :optional t :type boolean)
  (|save| :optional t :type (or boolean |SaveOptions|)))

(define-interface |ServerCapabilities| ()
  (|textDocumentSync| :optional t :type (or |TextDocumentSyncOptions| number))
  (|hoverProvider| :optional t :type boolean)
  (|completionProvider| :optional t :type |CompletionOptions|)
  (|signatureHelpProvider| :optional t :type |SignatureHelpOptions|)
  (|definitionProvider| :optional t :type (or boolean |DefinitionOptions|))
  (|referencesProvider| :optional t :type (or boolean |ReferenceOptions|))
  (|documentHighlightProvider| :optional t :type (or boolean |DocumentHighlightOptions|))
  (|documentSymbolProvider| :optional t :type boolean)
  (|workspaceSymbolProvider| :optional t :type boolean)
  (|codeActionProvider| :optional t
                        :type (or boolean |CodeActionOptions|))
  (|codeLensProvider| :optional t :type |CodeLensOptions|)
  (|documentFormattingProvider| :optional t :type boolean)
  (|documentRangeFormattingProvider| :optional t :type boolean)
  (|documentOnTypeFormattingProvider| :optional t
                                      :type |DocumentOnTypeFormattingOptions|)
  (|renameProvider| :optional t :type boolean)
  (|documentLinkProvider| :optional t :type |DocumentLinkOptions|)
  (|executeCommandProvider| :optional t :type |ExecuteCommandOptions|)
  (|selectionRangeProvider| :optional t
                            :type (or boolean
                                      |SelectionRangeRegistrationOptions|
                                      |SelectionRangeOptions|))
  (|workspace| :optional t :type t)
  (|experimental| :optional t :type t))

(define-interface |ShowMessageParams| ()
  (|type| :type number)
  (|message| :type string))

(define-interface |ShowDocumentParams| ()
  (|uri|
   :type uri
   :documentation "The document URI to show.")
  (|external|
   :optional t :type boolean
   :documentation "Show in an external program?")
  (|takeFocus|
   :optional t :type boolean
   :documentation "Give the new document focus?")
  (|selection|
   :optional t :type |Range|
   :documentation "Optional range if the document is a text document."))

(define-interface |ShowDocumentResult| ()
  (|success| :type boolean))

(define-namespaced-constants |MessageType| ()
  (|Error| 1)
  (|Warning| 2)
  (|Info| 3)
  (|Log| 4))

(define-interface |ShowMessageRequestParams| ()
  (|type| :type |MessageType|)
  (|message| :type string)
  (|actions| :optional t :type (proper-list |MessageActionItem|)))

(define-interface |MessageActionItem| ()
  (|title| :type string))

(define-interface |LogMessageParams| ()
  (|type| :type number)
  (|message| :type string))

(define-interface |Registration| ()
  (|id| :type string)
  (|method| :type string)
  (|registerOptions| :optional t))

(define-interface |RegistrationParams| ()
  (|registrations| :type (proper-list |Registration|)))

(define-interface |TextDocumentRegistrationOptions| ()
  (|documentSelector| :type (or |DocumentSelector| null)))

(define-interface |TextDocumentChangeRegistrationOptions| (|TextDocumentRegistrationOptions|)
  (|syncKind| :type number))

(define-interface |SignatureHelpRegistrationOptions|
    (|TextDocumentRegistrationOptions|
     |SignatureHelpOptions|))

(define-interface |Unregistration| ()
  (|id| :type string)
  (|method| :type string))

(define-interface |UnregistrationParams| ()
  (|unregisterations| :type (proper-list |Unregistration|)))

(define-interface |DidOpenTextDocumentParams| ()
  (|textDocument| :type |TextDocumentItem|))

(define-interface |DidChangeTextDocumentParams| ()
  (|textDocument| :type |VersionedTextDocumentIdentifier|)
  (|contentChanges| :type (proper-list |TextDocumentContentChangeEvent|)))

(define-interface |TextDocumentContentChangeEvent| ()
  (|range| :optional t :type |Range|)
  (|rangeLength| :optional t :type number)
  (|text| :type string))

(define-interface |DidSaveTextDocumentParams| ()
  (|textDocument| :type |TextDocumentIdentifier|)
  (|text| :optional t :type string))

(define-interface |DidCloseTextDocumentParams| ()
  (|textDocument| :type |TextDocumentIdentifier|))

(define-interface |FileDelete| ()
  (|uri| :type string))                 ;sic

(define-interface |DeleteFilesParams| ()
  (|files| :type (proper-list |FileDelete|)))

(define-interface |FileOperationRegistrationOptions| ()
  (|filters| :type (proper-list |FileOperationFilter|)))

(define-interface |FileOperationFilter| ()
  (|scheme| :optional t :type string)
  (|pattern| :type |FileOperationPattern|))

(define-namespaced-constants |FileOperationPatternKind| ()
  (|file| :|file|)
  (|folder| :|folder|))

(define-interface |FileOperationPatternOptions| ()
  (|ignoreCase| :optional t :type boolean))

(define-interface |FileOperationPattern| ()
  (|glob| :type string)
  (|matches| :optional t :type |FileOperationPatternKind|)
  (|options| :optional t :type |FileOperationPatternOptions|))

(define-interface |PublishDiagnosticsParams| ()
  (|uri| :type |DocumentUri|)
  (|diagnostics| :type (proper-list |Diagnostic|)))

(define-interface |CompletionList| ()
  (|isIncomplete| :type boolean)
  (|items| :type (proper-list |CompletionItem|)))

(define-namespaced-constants |InsertTextFormat| ()
  (|PlainText| 1)
  (|Snippet| 2))

(define-interface |CompletionItem| ()
  (|label| :type string)
  (|kind| :optional t :type number)
  (|detail| :optional t :type string)
  (|documentation| :optional t :type string)
  (|sortText| :optional t :type string)
  (|filterText| :optional t :type string)
  (|insertText| :optional t :type string)
  (|insertTextFormat| :optional t :type |InsertTextFormat|)
  (|textEdit| :optional t :type |TextEdit|)
  (|additionalTextEdits| :optional t :type (proper-list |TextEdit|))
  (|command| :optional t :type |Command|)
  (|data| :optional t :type t))

(define-namespaced-constants |MarkupKind| ()
  (|PlainText| :|plaintext|)
  (|Markdown| :|markdown|))

(eval-always
 (export
  (deftype |MarkedString| ()
    ;; The block variant of |MarkedString| has no interface in
    ;; LSP, so we just represent it as a hash table.
    '(or string hash-table))))

(define-interface |MarkupContent| ()
  (|kind| :type |MarkupKind|)
  (|value| :type string))

(defmethod initialize-instance :after ((instance |MarkupContent|) &key)
  (with-slots (|kind|) instance
    (when (stringp |kind|)
      (setf |kind|
            (or (serapeum:find-keyword |kind|)
                |kind|)))))

(define-interface |HoverParams| (|TextDocumentPositionParams|
                                 |WorkDoneProgressParams|))

(define-interface |Hover| ()
  (|contents| :type (or |MarkedString|
                        (proper-list |MarkedString|)
                        |MarkupContent|))
  (|range| :optional t :type |Range|))

(define-namespaced-constants |SignatureHelpTriggerKind| ()
  (|Invoked| 1)
  (|TriggerCharacter| 2)
  (|ContentChange| 3))

(define-interface |SignatureHelpContext| ()
  (|triggerKind| :type |SignatureHelpTriggerKind|)
  (|triggerCharacter| :optional t :type string)
  (|isRetrigger| :type boolean)
  (|activeSignatureHelp| :optional t :type |SignatureHelp|))

(define-interface |SignatureHelpParams|
    (|TextDocumentPositionParams|
     |WorkDoneProgressParams|)
  (|context| :optional t :type |SignatureHelpContext|))

(define-interface |SignatureHelp| ()
  (|signatures| :type (proper-list |SignatureInformation|))
  (|activeSignature| :optional t :type number)
  (|activeParameter| :optional t :type number))

(define-interface |SignatureInformation| ()
  (|label| :type string)
  (|documentation| :optional t :type string)
  (|parameters| :optional t :type (proper-list |ParameterInformation|)))

(define-interface |ParameterInformation| ()
  (|label| :type string)
  (|documentation| :optional t :type string))

(define-interface |ReferenceParams| (|TextDocumentPositionParams|)
 (|context| :type |ReferenceContext|))

(define-interface |ReferenceContext| ()
  (|includeDeclaration| :type boolean))

(define-interface |DocumentHighlight| ()
  (|range| :type |Range|)
  (|kind| :optional t :type number))

(define-namespaced-constants |DocumentHighlightKind| ()
  (|Text| 1)
  (|Read| 2)
  (|Write| 3))

(define-interface |DocumentSymbolParams| ()
  (|textDocument| :type |TextDocumentIdentifier|))

(define-interface |SymbolInformation| ()
  (|name| :type string)
  (|kind| :type number)
  (|location| :type |Location|)
  (|containerName| :optional t :type string))

(define-interface |DocumentSymbol| ()
  (|name| :type string)
  (|detail| :type string :optional t)
  (|kind| :type number)
  (|deprecated| :type boolean :optional t)
  (|range| :type |Range|)
  (|selectionRange| :type |Range|)
  (|children| :optional t :type (proper-list |DocumentSymbol|)))

(define-namespaced-constants |SymbolKind| ()
  (|File| 1)
  (|Module| 2)
  (|Namespace| 3)
  (|Package| 4)
  (|Class| 5)
  (|Method| 6)
  (|Property| 7)
  (|Field| 8)
  (|Constructor| 9)
  (|Enum| 10)
  (|Interface| 11)
  (|Function| 12)
  (|Variable| 13)
  (|Constant| 14)
  (|String| 15)
  (|Number| 16)
  (|Boolean| 17)
  (|Array| 18)
  (|Object| 19)
  (|Key| 20)
  (|Null| 21)
  (|EnumMember| 22)
  (|Struct| 23)
  (|Event| 24)
  (|Operator| 25)
  (|TypeParameter| 26))

(define-interface |WorkspaceSymbolParams| ()
  (|query| :type string))

(define-interface |CodeLensParams| (|WorkDoneProgressParams| |PartialResultParams|)
  (|textDocument| :type |TextDocumentIdentifier|))

(define-interface |CodeLens| ()
  (|range| :type |Range|)
  (|command| :optional t :type |Command|)
  (|data| :optional t :type t))

(define-interface |DocumentLinkParams| ()
  (|textDocument| :type |TextDocumentIdentifier|))

(define-interface |DocumentLink| ()
  (|range| :type |Range|)
  (|target| :optional t :type |DocumentUri|))

(define-interface |DocumentFormattingParams| ()
  (|textDocument| :type |TextDocumentIdentifier|)
  (|options| :type |FormattingOptions|))

(define-interface |FormattingOptions| ()
  (|tabSize| :type number)
  (|insertSpaces| :type boolean)
  ;; [key: string]: boolean | number | string;
  )

(define-interface |DocumentRangeFormattingParams| ()
  (|textDocument| :type |TextDocumentIdentifier|)
  (|range| :type |Range|)
  (|options| :type |FormattingOptions|))

(define-interface |DocumentOnTypeFormattingParams| ()
  (|textDocument| :type |TextDocumentIdentifier|)
  (|position| :type |Position|)
  (|ch| :type string)
  (|options| :type |FormattingOptions|))

(define-interface |RenameParams| (|TextDocumentPositionParams|
                                  |WorkDoneProgressParams|)
  (|newName| :type string))

(define-namespaced-constants |CodeActionKind| ()
  (|Empty| :||)
  (|QuickFix| :|quickfix|)
  (|Refactor| :|refactor|)
  (|RefactorExtract| :|refactor.extract|)
  (|RefactorInline| :|refactor.inline|)
  (|RefactorRewrite| :|refactor.rewrite|)
  (|Source| :|source|)
  (|SourceOrganizeImports| :|source.organizeImports|))

(define-interface |WorkDoneProgressOptions| ()
  (|workDoneProgress| :optional t :type boolean))

(define-interface |ReferenceOptions| (|WorkDoneProgressOptions|))

(define-interface |CodeActionOptions| (|WorkDoneProgressOptions|)
  (|codeActionKinds| :optional t :type (proper-list |CodeActionKind|)))

(define-interface |CodeActionRegistrationOptions|
    (|TextDocumentRegistrationOptions| |CodeActionOptions|))

(define-interface |CancelParams| ()
  (|id| :type (or integer string)))

(deftype |ProgressToken| () '(or number string))
(export '|ProgressToken|)

(define-interface |ProgressParams| ()
  (|token| :type |ProgressToken|)
  (|value| :type t))

(define-interface |WorkDoneProgressParams| ()
  (|workDoneToken| :optional t :type |ProgressToken|))

(define-interface |PartialResultParams| ()
  (|partialResultToken| :optional t :type |ProgressToken|))

(define-interface |ApplyWorkspaceEditParams| ()
  (|label| :optional t :type string)
  (|edit| :type |WorkspaceEdit|))

(define-namespaced-constants |CompletionTriggerKind| ()
  (|Invoked| 1)
  (|TriggerCharacter| 2)
  (|TriggerForIncompleteCompletions| 3))

(define-interface |CompletionContext| ()
  (|triggerKind| :type |CompletionTriggerKind|)
  (|triggerCharacter| :optional t :type string))

(define-interface |CompletionParams|
    (|TextDocumentPositionParams|
     |WorkDoneProgressParams|
     |PartialResultParams|)
  (|context| :type |CompletionContext| :optional t))

(define-interface |CodeActionContext| ()
  (|diagnostics| :type (proper-list |Diagnostic|))
  (|only| :optional t :type (proper-list |CodeActionKind|)))

(define-interface |CodeActionParams|
    (|WorkDoneProgressParams| |PartialResultParams|)
  (|textDocument| :optional t :type |TextDocumentIdentifier|)
  (|range| :optional t :type |Range|)
  (|context| :type |CodeActionContext|))

;;; This is implicit in the definition of CodeAction as of 3.17.
(define-interface |CodeActionDisabled| ()
  :static-validate nil
  (|reason| :type string))

(define-interface |CodeAction| ()
  (|title| :type string)
  (|kind| :optional t :type |CodeActionKind|)
  (|diagnostics| :optional t :type (proper-list |Diagnostic|))
  (|isPreferred| :optional t :type boolean)
  (|disabled| :optional t :type |CodeActionDisabled|)
  (|edit| :optional t :type |WorkspaceEdit|)
  (|command| :optional t :type |Command|))

(define-interface |ExecuteCommandParams| (|WorkDoneProgressParams|)
  (|command| :type string)
  (|arguments| :optional t :type list))

(define-interface |WorkDoneProgressBegin| ()
  (|kind| :type (eql :|begin|) :initform :|begin|)
  (|title| :type string)
  (|cancellable| :optional t :type boolean)
  (|message| :optional t :type string)
  (|percentage| :optional t :type number))

(define-interface |WorkDoneProgressReport| ()
  (|kind| :type (eql :|report|) :initform :|report|)
  (|cancellable| :optional t :type boolean)
  (|message| :optional t :type string)
  (|percentage| :optional t :type number))

(define-interface |WorkDoneProgressEnd| ()
  (|kind| :type (eql :|end|) :initform :|end|)
  (|message| :optional t :type string))

(define-interface |WorkDoneProgressCreateParams| ()
  (|token| :type |ProgressToken|))

(define-interface |WorkDoneProgressCancelParams| ()
  (|token| :type |ProgressToken|))

;;;
;;; StaticRegistrationOptions
;;;
(define-interface |StaticRegistrationOptions| ()
  (|id| :optional t :type string))

;;;
;;; selectionRange
;;;
(define-interface |SelectionRangeClientCapabilities| ()
  (|dynamicRegistration| :optional t :type boolean))

(define-interface |SelectionRangeOptions| (|WorkDoneProgressOptions|))

(define-interface |SelectionRangeRegistrationOptions|
    (|SelectionRangeOptions|
     |TextDocumentRegistrationOptions|
     |StaticRegistrationOptions|))

(define-interface |SelectionRangeParams|
    (|WorkDoneProgressParams| |PartialResultParams|)
  (|textDocument| :type |TextDocumentIdentifier|)
  (|positions| :type (proper-list |Position|)))

(define-interface |SelectionRange| ()
  (|range| :type |Range|)
  (|parent| :optional t :type |SelectionRange|))

(define-interface |LogTraceParams| ()
  (|message| :type string)
  (|verbose| :type string :optional t))

(define-namespaced-constants |TraceValue| ()
  (|off| :|off|)
  (|message| :|message|)
  (|verbose| :|verbose|))

(define-interface |SetTraceParams| ()
  (|value| :type |TraceValue|))

(define-interface |WorkspaceFolder| ()
  (|uri| :type |DocumentUri|)
  (|name| :type string))

(define-interface |WorkspaceFoldersChangeEvent| ()
  (|added| :type (proper-list |WorkspaceFolder|))
  (|removed| :type (proper-list |WorkspaceFolder|)))

(define-interface |DidChangeWorkspaceFoldersParams| ()
  (|event| :type |WorkspaceFoldersChangeEvent|))

(define-interface |ConfigurationParams| ()
  (|items| :type (proper-list |ConfigurationItem|)))

(define-interface |ConfigurationItem| ()
  (|scopeUri| :type |DocumentUri| :optional t)
  (|section| :type string :optional t))

(define-interface |DidChangeWatchedFilesClientCapabilities| ()
  (|dynamicRegistration| :type boolean :optional t))

(define-interface |DidChangeWatchedFilesParams| ()
  (|changes| :type (proper-list |FileEvent|)))

(define-namespaced-constants |FileChangeType| ()
  (|Created| 1)
  (|Changed| 2)
  (|Deleted| 3))

(define-interface |FileEvent| ()
  (|uri| :type |DocumentUri|)
  (|type| :type |FileChangeType|))

;;; Goto Definition Request
(define-interface |DefinitionClientCapabilities| ()
  (|dynamicRegistration| :type boolean :optional t)
  (|linkSupport| :type boolean :optional t))

(define-interface |DefinitionOptions| (|WorkDoneProgressOptions|))

(define-interface |TypeDefinitionOptions| (|WorkDoneProgressOptions|))

(define-interface |TypeDefinitionRegistrationOptions|
    (|TextDocumentRegistrationOptions| |TypeDefinitionOptions| |StaticRegistrationOptions|))

(define-interface |DefinitionParams| (|TextDocumentPositionParams|
                                      |WorkDoneProgressParams|
                                      |PartialResultParams|))

(define-interface |TypeDefinitionParams|
    (|TextDocumentPositionParams| |WorkDoneProgressParams| |PartialResultParams|))

;;; Find References Request
(define-interface |ReferenceClientCapabilities| ()
  (|dynamicRegistration| :type boolean :optional t))

(define-interface |ReferenceOptions| (|WorkDoneProgressOptions|))

(define-interface |ReferenceRegistrationOptions|
    (|TextDocumentRegistrationOptions| |ReferenceOptions|))

(define-interface |ReferenceParams|
    (|TextDocumentPositionParams| |WorkDoneProgressParams| |PartialResultParams|)
  (|context| :type |ReferenceContext|))

(define-interface |ReferenceContext| ()
  (|includeDeclaration| :type boolean))

;;; Document Highlights Request
(define-interface |DocumentHighlightClientCapabilities| ()
  (|dynamicRegistration| :type boolean :optional t))

(define-interface |DocumentHighlightOptions| (|WorkDoneProgressOptions|))

(define-interface |DocumentHighlightRegistrationOptions|
    (|TextDocumentRegistrationOptions| |ReferenceOptions|))

(define-interface |DocumentHighlightParams|
    (|TextDocumentPositionParams| |WorkDoneProgressParams| |PartialResultParams|))

(define-interface |DocumentHighlight| ()
  (|range| :type |Range|)
  (|kind| :type |DocumentHighlightKind| :optional t))

(define-namespaced-constants |TextDocumentSyncKind| ()
  (|Text| 1)
  (|Read| 2)
  (|Write| 3))


;;; Conversions

(defun protocol-symbol-p (type)
  "If TYPE is a protocol symbol, return it."
  (values (gethash type *protocol-symbols*)))

(defun protocol-list-p (type)
  (and (consp type)
       (member (car type) '(proper-list soft-list-of))
       (or (protocol-symbol-p (second type))
           (trivia:match (second type)
             ((list* 'or types)
              (every #'protocol-symbol-p types))))))

(defun protocol-map-p (type)
  "Is TYPE a literal JSON map type?"
  (trivia:match type
    ((list 'json-map _ _) t)))

(defun protocol-convert (type value)
  (cond
    ((and (subtypep type 'keyword)
          (find-keyword (string value))))
    ((and (symbolp type)
          (protocol-symbol-p type))
     (unless (hash-table-p value)
       (error 'type-error
              :expected-type 'hash-table
              :datum value))
     (convert-from-hash-table type value))
    ((protocol-list-p type)
     (unless (listp value)
       (error 'type-error
              :expected-type 'list
              :datum value))
     (mapcar (lambda (hash-value-1)
               (protocol-convert (second type) hash-value-1))
             value))
    ((protocol-map-p type)
     (ematch type
       ((list 'json-map (list _ from-type) to-type)
        (let ((out
               (make-hash-table
                :test (hash-table-test value)
                :size (hash-table-size value))))
          (maphash (lambda (k v)
                     (let ((k (protocol-convert from-type k))
                           (v (protocol-convert to-type v)))
                       (setf (gethash k out) v)))
                   value)
          out))))
    ;; Alternations and converting to symbols are tricky, because we
    ;; want to fall through if there is an error but succeed when the
    ;; result of the conversion is nil.
    ((match type
       ((list* 'or subtypes)
        (dolist (alt subtypes)
          (ignore-some-conditions (type-error required-argument)
            (return-from protocol-convert
              (protocol-convert alt value)))))))
    ((typep value type) value)
    ((and (symbolp type)
          (ignore-errors
            (return-from protocol-convert
              (convert type value)))))
    (t
     (error 'type-error
            :expected-type type
            :datum value))))

(defun convert-from-hash-table* (name hash-table slot-specs)
  "Helper function for `convert-from-hash-table'.
The implements the default strategy for creating an instance from a
hash table, by iterating over an alist of (slot-name . slot-type)
pairs.

This function will signal errors if any required slots in an instance
of class NAME cannot be set from the hash table."
  (let* ((class (find-class name))
         (object (allocate-instance class)))
    (assert (typep object 'protocol))
    (setf (slot-value object 'hash-table) hash-table)
    (handler-bind ((type-error
                    (lambda (e)
                      (when (typep (type-error-datum e) 'null)
                        (invoke-restart 'skip-slot)))))
      (initialize-slots-from-hash-table object hash-table slot-specs))
    ;; At this point, any unbound required slots will signal errors.
    (initialize-instance object)
    object))

(defun initialize-slots-from-hash-table (object hash-table slot-specs)
  (loop for (slot-name . slot-type) in slot-specs
        for hash-key := (string slot-name)
        unless (slot-boundp object slot-name)
          do (multiple-value-bind (hash-value present?)
                 (gethash hash-key hash-table)
               (when present?
                 (tagbody
                    (restart-case
                        (setf (slot-value object slot-name)
                              (protocol-convert slot-type hash-value))
                      (skip-slot ()
                        (go :continue)))
                  :continue)))))

(defmethod shared-initialize :before
    ((inst protocol) (slot-names list) &rest initargs &key)
  (declare (ignore initargs))
  (when (slot-boundp inst 'hash-table)
    (handler-bind ((type-error
                    (lambda (e)
                      (when (typep (type-error-datum e) 'null)
                        (invoke-restart 'skip-slot)))))
      (initialize-slots-from-hash-table
       inst
       (slot-value inst 'hash-table)
       (remove-if-not (lambda (spec)
                        (member (car spec) slot-names))
                      (slot-specs inst))))))

(defun convert-to-hash-table* (instance slot-specs)
  "Helper function for `convert-to-hash-table'.
The implements the default strategy for converting an instance to a
hash table by iterating over an alist of (slot-name . slot-type)
pairs."
  (let ((hash-table (make-hash-table
                     :test 'equal
                     ;; Minimize the amount of wasted space.
                     :size (length slot-specs))))
    (loop :for (name . type) in slot-specs
          :for bound? := (slot-boundp instance name)
          :for value := (and bound? (slot-value instance name))
          :do (when bound?
                (setf (gethash (string name) hash-table)
                      (cond
                        ((keywordp value) (string value))
                        ((protocol-symbol-p type)
                         (convert-to-hash-table value))
                        ((and (protocol-list-p type) (listp value))
                         (mapcar #'convert-to-hash-table value))
                        ((and (consp type)
                              (eq 'or (car type))
                              (typep value 'protocol))
                         (convert-to-hash-table value))
                        (t
                         value)))))
    hash-table))

(defmethod convert-to-hash-table :before ((instance |Command|))
  (when (slot-boundp instance '|arguments|)
    (when (and (slot-value instance '|arguments|)
               (not (typep (slot-value instance '|arguments|) 'hash-table)))
      (setf (slot-value instance '|arguments|)
            (mapcar #'convert-to-hash-table (slot-value instance '|arguments|))))))

(defmethod yason:encode ((self protocol) &optional (s *standard-output*))
  "Encode a protocol object directly to JSON.

This way we can use the interface's known slot types to ensure that
the JSON output is valid. Notably a false boolean should render as
`false', not `null', and an empty list should render as `[]', not
`null'."
  (yason:with-output (s)
    (yason:with-object ()
      (let ((slot-specs (slot-specs/cached self)))
        (loop for (name . type) in slot-specs
              for bound? = (slot-boundp self name)
              for value = (and bound? (slot-value self name))
              when bound? do
                (nlet encode (value)
                  (cond ((keywordp value)
                         (encode (string value)))
                        ((and (protocol-list-p type) (null value))
                         (encode #()))
                        ((and (subtypep type 'boolean)
                              (typep value 'boolean))
                         (encode (if value 'yason:true 'yason:false)))
                        (t (yason:encode-object-element
                            (string name)
                            value)))))))))


;;; Capabilities

(defun parse-lsp-path (x)
  "Parse a dot-separated LSP property path."
  (etypecase x
    (string
     (if (find #\/ x)
         (error "This is an LSP method, not a path: ~a" x)
         (loop for part in (split-sequence #\. x)
               collect (intern part :lsp-server/protocol))))
    (list x)))

(defun lookup-lsp-path (caps path &optional optional?)
  "Lookup PATH (a string or a list of symbols) in CAPS.
CAPS can be an instance of `protocol' or a hash table.

If OPTIONAL? is provided then it is returned if the path does not
exist."
  (check-type caps
              (or |ClientCapabilities| |ServerCapabilities|
                  hash-table))
  (flet ((missing (caps slot)
           (if optional?
               (return-from lookup-lsp-path nil)
               (error "Missing required slot ~a in ~a" slot caps))))
    (reduce (lambda (caps slot)
              (multiple-value-bind (value bound?)
                  (typecase caps
                    (hash-table
                     (gethash (string slot) caps))
                    (protocol
                     (slot-value-safe caps slot))
                    (otherwise))
                (if (not bound?)
                    (missing caps slot)
                    value)))
            (parse-lsp-path path)
            :initial-value caps)))

(defun method-path (method table)
  "Get the path for looking up METHOD in table.
Return T as a second value if the capability is optional."
  (when-let (cap (gethash method table))
    (values (parse-lsp-path (cap-path cap))
            (cap-optional? cap))))

(defun client-method-path (method)
  (method-path method *client-caps*))

(defun server-method-path (method)
  (method-path method *server-caps*))

(defun server-caps-support (server-caps method)
  "Return support for METHOD in SERVER-CAPS.

For a method that must be supported, or can be freely ignored, just
returns SERVER-CAPS.

If the property type is a boolean, this can be treated as a predicate,
but if not you may have to introspect the return value to determine
what exactly is supported."
  (check-type server-caps |ServerCapabilities|)
  (multiple-value-call #'lookup-lsp-path server-caps
    (server-method-path method)))

(defun client-caps-support (client-caps method)
  "Return support for METHOD in CLIENT-CAPS.

Cf. `server-support'."
  (check-type client-caps |ClientCapabilities|)
  (multiple-value-call #'lookup-lsp-path client-caps
    (client-method-path method)))
