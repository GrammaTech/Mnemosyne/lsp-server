(defpackage :lsp-server/state
  (:use :cl)
  (:export :*server*))
(in-package :lsp-server/state)

(declaim (special *server*))
